# Backend for BPS

## Introduction

A repository that provides a nodejs server code base that integrates with mysql. Hence, calling it **MyNodeSQL** because it combines `MySQL` + `nodejs`.
It provides a template to enable new API endpoints and a standard to document these API endpoints on 3rd party website such as Swagger.
The goal of this design is to provide a simple framework to setup a nodejs engine that can interact with a MySQL cluster with pool connections. The vision is to enhance the design to make it more flexible to add new routes and business logics without restarting `nodejs` engine. Today, you will need to restart nodejs everytime when you add a new route or update the business logic.

A simple illustration on how this works is shwon here.

```text
            limiter +---> htaccess          +---> route ---+
               ^             |              |              |
               |             |              |              |
               |             |              |              |
client ---> request          +---> dispatch +---> auth --->+
  ^                                                        |
  |                                                        |
  |                                                        |
  +-----------------------serverError <--- clientError <---+

```

### Authentication

Authentication utilize 2 methods, basic authentication and JWT. Default is set to basic authentication.

- **Authentication Disabled in x.x.4 versions**

  For each semver version, we choose the patch release `4` (e.g. `0.0.4`, `0.1.4`, `0.2.4`, etc) to disbale JWT authentication on purpose to allow others to query without any authentication for their convinience. This is a temporarily hack to allow queries bypass authentication for others to test before they can provide their proper test case to simulate user login from the front-end, and there is no gurantee this will be available for all branches.

- **Configuration**

  In order to switch to a different authentication method, you can utilize the env variable `AUTH_TYPE`. Supported values are:

  - `ALL`: Be careful, if you enable all authentication methods, some may override each other and result in weird behaviors. Not recommended.
  - `BASIC_AUTHENTICATION`: Basic authentication with JWT token.
  - `ANONYMOUS`: (Not supported in BPS project) JWT token for any user access. Only validate the JWT token format and track uuid and user's IP.

  For example,

  ```bash
  # Enabling all authentication that we support
  export AUTH_TYPE=ALL
  NODE_ENV=test npm start
  ```

  ```bash
  # Enabling ANONYMOUS authentication
  unset AUTH_TYPE
  export AUTH_TYPE=ANONYMOUS
  NODE_ENV=test npm start
  ```

- **Basic Authentication**

  We use this for basic user operation such as signup, login, update user profiles, etc. which requires us to ask the user to provide their `password` again before updating their profile including `password` itself.

- **JWT**

  We use this for all other sub-sequent calls to backend services. You will need to use HTTP/HTTPS request header `Authorization: Bearer ...`
  after the user login.

- **Adding your own Authentication Model and Passport Strategy**

  If you want to extend the current customized passport strategy amd models to track logins, you will need to update 2 places:

  - `./src/config/authentication.json`: Extend the existing `AUTH_TYPE` with your own key-value pair and mapping.
  - `./src/models/authentication/<YOUR_AUTH_MODEL_DIR>`: Add you authentication model module in its own directory here. Since it is a module, define your own `index.js` to load them.
  - `./src/custom-passport`: Add your own customized passport strategy and implementation.
  - `./src/app.js`: The last part, you will need to manually update and include you customized passport stratey in `./scr/custom-passport`. We haven't update this to make it automatic yet.

### Anonymous Tracking

This does not require any authentication, but a simple JWT token to track the client via `uuidv4`. A record is recorded by an `uuidv4` + client's IP address.

### Query API Authentication