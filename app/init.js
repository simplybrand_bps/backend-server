'use strict'
/*
* 服务器网络声明，应该在此文件中
*/
import http from 'http'

import { serverHandle, app } from './src'

try {
  serverHandle.serverPort()
    .then(port => {
      const server = http.createServer(app)
      server.listen(port)

      server.on('error', serverHandle.serverStatus.onError)
      server.on('listening', () => serverHandle.serverStatus.onListening(server.address()))
    })
} catch (e) {
  serverHandle.initError(e)
}
