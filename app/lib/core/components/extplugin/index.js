/**
 * 配置可用模块
 */
import info from './info'
import project from './project'

export default {
    info,
    project
}