import { sqlquery } from '../../../data'

const baseData = sqlquery.baseData

const addInfo = async (docs) => {
  let dataSql = 'INSERT INTO `task`(`name`, `brandId`, `channel`, `industry`,`category`, `series`, `model`, `status`) VALUES '
  dataSql += `('${docs.name}', '${docs.brandid}',  '${docs.channel}', '${docs.industry}','${docs.category}', '${docs.series}','${docs.model}', 1)`

  const dataResult = await baseData(dataSql)

  let baseSql = 'INSERT INTO `policy`(`name`, `mainId`, `packageId`, `init`, `expire`, `subid`,`status`,`userid`) VALUES '
  baseSql += `('${docs.name}', '${docs.mainid}', '${docs.packageid}', '${docs.init}', '${docs.expire}', '${dataResult.insertId}', 1,'${docs.userid}')`
  const baseResult = await baseData(baseSql)

  let extSql = 'INSERT INTO `contract`(`brandid`, `poid`, `taskid`) VALUES '
  extSql += `('${docs.brandid}', '${baseResult.insertId}', '${dataResult.insertId}')`
  await baseData(extSql)
  /**
   * plugin
   */
  // 0、获取详细品牌信息
  const step0 = 'SELECT * FROM brandlist WHERE `brandId` = ' + `'${docs.brandid}'`
  // 1、获取品牌ID
  const step1 = 'SELECT * FROM `anti_counterfeiting_test`.`BrandList` WHERE `posubid` = ' + `'${docs.brandid}'`
  const stepResult0 = await baseData(step0)
  const stepResult1 = await baseData(step1)

  // 2、插入task任务
  let step2 = 'INSERT INTO `anti_counterfeiting_test`.`project_config`(`brandId`, `name`, `Industry`, `Category`, `Series`, `Model`, `project`) '
  step2 += `VALUES ('${stepResult1[0].BrandId}','${docs.name}','${docs.industry}','${docs.category}', '${docs.series}','${docs.model}', '${dataResult.insertId}')`
  const stepResult2 = await baseData(step2)
  // 3、插入渠道关键词
  let step3 = 'INSERT INTO `anti_counterfeiting_test`.`project_keyword`(`projectId`, `keyword`, `channel`) VALUES '
  const stepWords = stepResult0[0].keywords.split(",")
  stepWords.forEach(stepdoc => {
    if(stepdoc) step3 += `('${stepResult2.insertId}','${stepdoc}','${docs.channel}'),`
  })
  await baseData(step3.substring(0,step3.length-1))
  // 4、插入品牌关联
  let step4 = 'INSERT INTO `anti_counterfeiting_test`.`project_brand`(`projectId`, `brandId`) VALUES '
  step4 += `('${stepResult2.insertId}','${stepResult1[0].BrandId}')`
  await baseData(step4)
  // 5、插入项目配置
  let step5 = 'INSERT INTO `bps_user`.`monitorconfig`(`Industry`, `BrandName`, `Channel`, `Series`, `Model`, `Class`, `projectId`) VALUES '
  step5 += `('${docs.industry}','${docs.name}','${docs.channel}', '${docs.series}','${docs.model}', '${docs.category}','${dataResult.insertId}')`
  const mConf = await baseData(step5)
  // 6、生成套餐
  let step6 = 'INSERT INTO `bps_test`.`policy`(`mainId`, `status`, `type`, `init`, `expire`, `name`) VALUES '
  const baselevel = parseInt(docs.packageid) ? parseInt(docs.packageid) + 4 : 5
  step6 += `('${docs.mainid}',1,'${baselevel}','${docs.init}', '${docs.expire}','${docs.name}')`
  const packResult = await baseData(step6)
  // relation
  let step06 = 'INSERT INTO `bps_test`.`potask` (`packageId`, `taskid`, `mainId`, `poid`) VALUES '
  step06 += `('${packResult.insertId}','${stepResult2.insertId}','${docs.mainid}', '${baseResult.insertId}')`
  await baseData(step06)
  const extdataid = docs.mainid == 6? 0: 6
  // ext
  let ext1 = 'INSERT INTO `bps_test`.`policy`(`mainId`, `status`, `type`, `init`, `expire`, `name`) VALUES '
  ext1 += `('${extdataid}',1,'${baselevel}','${docs.init}', '${docs.expire}','${docs.name}')`
  const extpackResult = await baseData(ext1)
  // relation
  let ext0 = 'INSERT INTO `bps_test`.`potask` (`packageId`, `taskid`, `mainId`, `poid`) VALUES '
  ext0 += `('${extpackResult.insertId}','${stepResult2.insertId}','${extdataid}', '${baseResult.insertId}')`
  await baseData(ext0)

  // 7、生成项目
  let step7 = 'INSERT INTO `bps_test`.`project`(`packageId`, `status`, `type`, `mainId`, `step`) '
  step7 += `VALUES ('${packResult.insertId}',3,3,'${docs.mainid}',3)`
  const proResult = await baseData(step7)
  // ext
  let ext2 = 'INSERT INTO `bps_test`.`project`(`packageId`, `status`, `type`, `mainId`, `step`) '
  ext2 += `VALUES ('${extpackResult.insertId}',3,3,'${extdataid}',3)`
  const extproResult = await baseData(ext2)
  // 8、记录项目信息
  let step8 = 'INSERT INTO `bps_test`.`content`(`projectId`, `name`, `content`) VALUES '
  step8 += `('${proResult.insertId}', 'project', '${mConf.insertId}')`
  await baseData(step8)
  // ext
  let ext3 = 'INSERT INTO `bps_test`.`content`(`projectId`, `name`, `content`) VALUES '
  ext3 += `('${extproResult.insertId}', 'project', '${mConf.insertId}')`
  await baseData(ext3)
  // 9、其它模块配置
  if (parseInt(docs.packageid) > 1) {
    let step07 = 'INSERT INTO `bps_test`.`project`(`packageId`, `status`, `type`, `mainId`, `step`) '
    step07 += `VALUES ('${packResult.insertId}',3,1,'${docs.mainid}',3)`
    const mproResult = await baseData(step07)
    let step08 = 'INSERT INTO `bps_test`.`content`(`projectId`, `name`, `content`) VALUES '
    step08 += `('${mproResult.insertId}', 'project', '${stepResult2.insertId}')`
    await baseData(step08)
    // ext
    let ext02 = 'INSERT INTO `bps_test`.`project`(`packageId`, `status`, `type`, `mainId`, `step`) '
    ext02 += `VALUES ('${extpackResult.insertId}',3,1,'${extdataid}',3)`
    const extproResult = await baseData(ext02)
    let ext03 = 'INSERT INTO `bps_test`.`content`(`projectId`, `name`, `content`) VALUES '
    ext03 += `('${extproResult.insertId}', 'project', '${stepResult2.insertId}')`
    await baseData(ext03)
  }
  if (parseInt(docs.packageid) > 2) {
    let step007 = 'INSERT INTO `bps_test`.`project`(`packageId`, `status`, `type`, `mainId`, `step`) '
    step007 += `VALUES ('${packResult.insertId}',1,2,'${docs.mainid}',3)`
    const nproResult = await baseData(step007)
    let step008 = 'INSERT INTO `bps_test`.`content`(`projectId`, `name`, `content`) VALUES '
    step008 += `('${nproResult.insertId}', 'project', '${mConf.insertId}')`
    await baseData(step008)
    // ext
    let ext002 = 'INSERT INTO `bps_test`.`project`(`packageId`, `status`, `type`, `mainId`, `step`) '
    ext002 += `VALUES ('${extpackResult.insertId}',1,2,'${extdataid}',3)`
    const extproResult = await baseData(ext002)
    let ext003 = 'INSERT INTO `bps_test`.`content`(`projectId`, `name`, `content`) VALUES '
    ext003 += `('${extproResult.insertId}', 'project', '${mConf.insertId}')`
    await baseData(ext003)
  }
  /**
   * plugin
   */ 
  await baseData(extSql)

  return docs
}

const editInfo = async (docs) => {
  let dataSql = 'UPDATE task SET `name` = '
  dataSql += `'${docs.name}', channel = '${docs.channel}', category = '${docs.category}', 
  series = '${docs.series}' , model = '${docs.model}' WHERE id = ${docs.subid}`
  await baseData(dataSql)
  /**
   * plugin
   */ 
  const baselevel = parseInt(docs.packageid) ? parseInt(docs.packageid) + 4 : 5
  let ext = 'UPDATE `bps_test`.`policy` a, `bps_test`.`potask` b, SET a.`name` = '
  ext += `'${docs.name}', a.init = '${docs.init}', a.expire = '${docs.expire}',  
  packageId = ${baselevel}  WHERE a.id = b.packageid AND b.taskid = ${docs.subid}`
  /**
   * plugin
   */ 
  return docs
}

const delInfo = async (docs) => {
  let ext = 'UPDATE `bps_test`.`policy` a, `bps_test`.`potask` b SET a.`mainId` = '
  if (docs.status) ext += `b.mainId WHERE a.id = b.packageid AND b.poid = ${docs.poid}`
  else ext += `0 WHERE a.id = b.packageid AND b.poid = ${docs.poid}`

  await baseData(ext)

  return docs
}

export default {
  addInfo,
  editInfo,
  delInfo
}