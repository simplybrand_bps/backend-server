import service from './dal'

const editInfo = async (docs) => {
  const data = await service.editInfo(docs)
  return data
}

const addInfo = async (docs) => {
  const data = await service.addInfo(docs)
  return data
}

const delInfo = async (docs) => {
  const data = await service.delInfo(docs)
  return data
}

export default {
  editInfo,
  addInfo,
  delInfo
}