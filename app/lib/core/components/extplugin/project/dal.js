import { sqlquery } from '../../../data'

const userData = sqlquery.userData

const infoList = async (docs) => {
  let dataSql = `SELECT * FROM monitorconfig `
  const searchStr =  `WHERE BrandName LIKE '%${docs.search}%' `
  if (docs.search) dataSql += searchStr
  if (docs.size) dataSql += `LIMIT ${docs.skip}, ${docs.size}`
  const result = await userData(dataSql)
  return result
}

const infoCount = async (docs) => {
  let dataSql = `SELECT count(*) AS Count FROM monitorconfig `
  const searchStr =  `WHERE BrandName LIKE '%${docs.search}%' `
  if (docs.search) dataSql += searchStr
  const result = await userData(dataSql)
  return result
}

export default {
  infoList,
  infoCount
}
