/**
 * 数据访问层，只于数据打交到，其它都不管
 * DAL层里不处理错误，在一个专门的对象里面处理错误
 */
import { localDB } from '../../../data'

const Dal = localDB.dbquery
const userDal = Dal('Account', localDB.db)

const getByAccount = async (docs) => {
  /**
   *查询数据库内相关字段
   * @param {String} username
   * @param {String} Status
   */
  const query = (({
    username
  }) => ({
    username
  }))(docs)
  if (docs.status) query.Status = 'active'
  const userInfo = await userDal.get(query)
  if (userInfo) return { userid: userInfo.id }
  return userInfo
}

export default {
  getByAccount
}
