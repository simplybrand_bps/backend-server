/**
 * services 层纯业务，没有与 http 相关的任何东西
 * 需要数据请调用 dal 中的方法
 * service 则是业务核心，不受controllers 和 dal等业务边界的影响
 */

import service from './dal'

const checkAccount = async (username) => {
  if (!username) throw new Error('Error happens with the status code: 502, the key is: username')
  const query = { username, status: 'active' }
  const userInfo = await service.getByAccount(query)
  return userInfo
}

export default {
  checkAccount
}
