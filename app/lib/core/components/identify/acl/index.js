import { auth as jwt } from '../../../utils'
import nconf from 'nconf'
import service from './dal'

const paramsFormat = (bodyParams) => {
  const paramHandler = {
    get: (target, key) => {
      if (target[key]) return target[key]
      else throw new Error(`Error happens with the status code: 503, the key is: ${key}`)
    },
    set: (target, key, value) => {
      target[key] = value
    }
  }
  const bodyParse = new Proxy(bodyParams, paramHandler)
  return bodyParse
}

const createJWT = async (docs) => {
  const params = paramsFormat(docs)
  const authConfig = await nconf.get('JWT')
  const tokenInfo = await jwt.genJwtToken(docs.account, authConfig)
  const query = (({
    account,
    userid
  }) => ({
    account,
    userid,
    mainid: docs.mainid,
    expire: authConfig.expiresIn
  }))(params)
  await service.createJWT(Object.assign(query, tokenInfo))
  return tokenInfo
}
/**
 * 登录验证
 */
const checkJWT = async (token) => {
  if (!token) throw new Error('Error happens with the status code: 401, the key is: token')
  const authToken = token.split(' ')
  if (!authToken.length || authToken[0] !== 'Bearer' || !authToken[1].length) throw new Error('Error happens with the status code: 401, the key is: token')
  const authConfig = await nconf.get('JWT')
  const jwtInfo = await jwt.verifyJWT(authToken[1], authConfig)
  // 这里直接更新过期时间
  const checkInfo = await service.resetJWT(jwtInfo)
  if (!checkInfo) throw new Error('Error happens with the status code: 401, the key is: token')
  return checkInfo
}

export default {
  createJWT,
  checkJWT
}
