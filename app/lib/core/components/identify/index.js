/**
 * 配置可用模块
 */
import acl from './acl'
import password from './password'
import account from './account'

export default {
  acl,
  password,
  account
}
