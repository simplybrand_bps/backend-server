/**
 * 数据访问层，只于数据打交到，其它都不管
 * DAL层里不处理错误，在一个专门的对象里面处理错误
 */
import { localDB } from '../../../data'

const Dal = localDB.dbquery
const passwordDal = Dal('Password', localDB.db)

const getByID = async (docs) => {
  /**
   *查询数据库内相关字段
   * @param {Number} id
   */
  const query = (({
    userId
  }) => ({
    userId
  }))(docs)

  const userInfo = await passwordDal.get(query)
  return userInfo
}

const putPwd = async (docs) => {
  /**
   *查询数据库内相关字段
   * @param {Number} id
   */
  const data = (({
    password,
    salt
  }) => ({
    password,
    salt
  }))(docs)
  // 直接对信息进行更新，并返回更新后的信息
  const query = (({
    userId
  }) => ({
    userId
  }))(docs)
  const updateinfo = await passwordDal.put(query, data)
  return updateinfo
}

export default {
  getByID,
  putPwd
}
