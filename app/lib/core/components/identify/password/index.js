import { gen } from '../../../utils'
import nconf from 'nconf'
import service from './dal'

const paramsFormat = (bodyParams) => {
  const paramHandler = {
    get: (target, key) => {
      if (target[key]) return target[key]
      else throw new Error(`Error happens with the status code: 502, the key is: ${key}`)
    },
    set: (target, key, value) => {
      target[key] = value
    }
  }
  const bodyParse = new Proxy(bodyParams, paramHandler)
  return bodyParse
}

const checkPwd = async (docs) => {
  const params = paramsFormat(docs)
  const query = (({
    userid,
    password
  }) => ({
    userId: userid,
    password
  }))(params)
  const userInfo = await service.getByID(query)
  return gen.checkPwd(query.password, userInfo.password)
}

/**
 * 修改密码
 * @param {Number} userid
 * @param {String} password
 */
const putPwd = async (docs) => {
  const params = paramsFormat(docs)
  const query = (({
    userid,
    password
  }) => ({
    userId: userid,
    userPassword: password
  }))(params)

  const bcryptConfig = await nconf.get('bcrypt')
  const tokenConfig = await nconf.get('passwordToken')
  const tokenInfo = gen.genPassword(docs.password, bcryptConfig.salt)
  tokenInfo.expired = new Date(new Date().getTime() + tokenConfig.expiresIn * 1000)
  await service.putPwd(Object.assign(query, tokenInfo))
  return tokenInfo
}

/**
 * 生成激活信息
 * @param {String} password
 */
const genPwd = async (password) => {
  const bcryptConfig = await nconf.get('bcrypt')
  const tokenConfig = await nconf.get('activationToken')
  const tokenInfo = gen.genPassword(password, bcryptConfig.salt)
  tokenInfo.expired = new Date(new Date().getTime() + tokenConfig.expiresIn * 1000)
  return tokenInfo
}

const getByID = async (userid) => {
  const query = (({
    userid
  }) => ({
    userId: userid
  }))(params)
  const userInfo = await service.getByID({ userid })
  return userInfo
}

export default {
  checkPwd,
  putPwd,
  genPwd,
  getByID
}
