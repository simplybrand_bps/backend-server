/**
 * 配置可用模块
 */
import identify from './identify'
import user from './user'
import monitor from './monitor'
import system from './system'
import subscription from './subscription'
import extplugin from './extplugin'

/**
 * ToDo
 * 建立目录，直观表现每个模块注册的路由
 */

export default {
  identify,
  user,
  monitor,
  system,
  subscription,
  extplugin
}
