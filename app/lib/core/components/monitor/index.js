/**
 * 配置可用模块
 */
import info from './info'
import project from './project'
import result from './result'
import rule from './rule'

export default {
    info,
    project,
    result,
    rule
}