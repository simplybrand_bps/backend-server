import { sqlquery } from '../../../data'

const monitorData = sqlquery.monitorData

const infoList = async (docs) => {
  let dataSql = `SELECT a.*, d.Unchecked FROM infoname a 
  LEFT JOIN matcheck d ON a.id = d.SkuId WHERE a.isDel = 0 `
  if (docs.projectId) dataSql += `AND a.projectId = ${docs.projectId} `
  if (docs.search) dataSql += `AND (a.productName LIKE '%${docs.search}%' 
  OR a.id LIKE '%${docs.search}%' OR a.skucode LIKE '%${docs.search}%')`
  if (docs.size) dataSql += `LIMIT ${docs.skip}, ${docs.size}`
  const result = await monitorData(dataSql)
  return result
}

const infoCount = async (docs) => {
  let dataSql = `SELECT count(*) AS Count FROM infoname WHERE isDel = 0 `
  if (docs.projectId) dataSql += `AND projectId = ${docs.projectId} `
  if (docs.search) dataSql += `AND (productName LIKE '%${docs.search}%' 
  OR id LIKE '%${docs.search}%' OR skucode LIKE '%${docs.search}%')`
  const result = await monitorData(dataSql)

  return result
}

const infoView = async (docs) => {
  let dataSql = `SELECT * FROM infoname WHERE id = ${docs.skuid} `
  const result = await monitorData(dataSql)
  return result
}

const addInfo = async (docs) => {
  let dataSql = `INSERT INTO skubaseinfo (TaskID, SKUDesc, SKUName, SKUSeries, 
    SKUModel, SKUShortName, SKUSpec, UserSKUCode, price) VALUES (
    '${docs.projectId}', '${docs.description}', '${docs.productName}', 
    '${docs.series}', '${docs.model}', '${docs.shortName}', 
    '${docs.specification}', '${docs.skucode}', '${docs.price}')`
  const result = await monitorData(dataSql)

  return result
}

const editInfo = async (docs) => {
  const dataSql = `UPDATE skubaseinfo SET SKUDesc = '${docs.descripition}',
  SKUName = '${docs.productName}', SKUSeries = '${docs.series}', 
  SKUModel = '${docs.model}', SKUShortName = '${docs.shortName}', 
  SKUSpec = '${docs.specification}', UserSKUCode = '${docs.skucode}',
  price = '${docs.price}' WHERE SKUID = ${docs.id}`
  await monitorData(dataSql)

  return docs
}

const setPrice = async (docs) => {
  let dataSql = `UPDATE skubaseinfo SET
  price = '${docs.price}' WHERE SKUID = ${docs.id}`
  await monitorData(dataSql)

  return docs
}

const delInfo = async (docs) => {
  const dataSql = `UPDATE skubaseinfo SET isDel = 1 WHERE SKUID = ${docs.id}`
  await monitorData(dataSql)

  return docs
}

export default {
  infoList,
  infoCount,
  addInfo,
  editInfo,
  delInfo,
  setPrice,
  infoView
}