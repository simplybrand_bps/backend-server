import service from './dal'
import { gen } from '../../../utils'

const addInfo = async (docs) => {
  docs.token = gen.md5(`'${docs.projectId}', '${docs.productName}', '${docs.series}', 
  '${docs.model}', '${docs.category}', '${docs.shortName}', '${docs.specification}'`)

  const data = await service.addInfo(docs)
  return data
}

const editInfo = async (docs) => {
  const data = await service.editInfo(docs)
  return data
}

const infoList = async (docs) => {
  const data = await service.infoList(docs)
  return data
}

const infoCount = async (docs) => {
  const data = await service.infoCount(docs)
  return data
}

const delInfo = async (docs) => {
  const data = await service.delInfo(docs)
  return data
}

const setPrice = async (docs) => {
  const data = await service.setPrice(docs)
  return data
}

const infoView = async (docs) => {
  const data = await service.infoView(docs)
  return data
}

export default {
    infoList,
    infoCount,
    addInfo,
    editInfo,
    delInfo,
    setPrice,
    infoView
}