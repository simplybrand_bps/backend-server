import { sqlquery } from '../../../data'

const monitorData = sqlquery.monitorData

const infoList = async (docs) => {
  let dataSql = `SELECT * FROM predata WHERE TaskId = ${docs.subid} AND `
  if (docs.type) dataSql += ' NOT (1 = 1 '
  else dataSql += ' (1 = 1 '
  if (docs.keep) dataSql += 'AND `AssetTitle` REGEXP '+ `'${docs.keep}' `
  if (docs.filter) dataSql += 'AND `AssetTitle` NOT REGEXP '+ ` '${docs.filter}' `
  dataSql += `) LIMIT ${docs.skip}, ${docs.size}`
  const result = await monitorData(dataSql)
  return result
}

const infoCount = async (docs) => {
  let dataSql = `SELECT count(*) AS Count FROM predata WHERE TaskId = ${docs.subid} AND `
  if (docs.type) dataSql += ' NOT (1 = 1 '
  else dataSql += ' (1 = 1 '
  if (docs.keep) dataSql += 'AND `AssetTitle` REGEXP '+ `'${docs.keep}' `
  if (docs.filter) dataSql += 'AND `AssetTitle` NOT REGEXP '+ ` '${docs.filter}' `
  dataSql += ') '
  const result = await monitorData(dataSql)
  return result
}

const infoDel = async (docs) => {
  let sql = `UPDATE assetdata SET isDel = 1 WHERE AssetID IN (${docs.idlist.toString()})`
  await monitorData(sql)

  let dataSql = 'INSERT INTO assetsku (`AssetID`, `TaskId`, `SkuId`,`CheckTime`) '
  dataSql += ` VALUES ${docs.content} ON DUPLICATE KEY UPDATE SkuId = -1, CheckTime = NOW() `
  await monitorData(dataSql)

  return docs
}

const linkList = async (docs) => {
  let dataSql = `SELECT * FROM v_link_list WHERE TaskId > 0 `
  if (docs.subid) dataSql += `AND SubscriptionID = ${docs.subid} `
  if (docs.skuid) dataSql += `AND SkuId = ${docs.skuid} `
  // if (docs.bansub) dataSql += `AND SkuId < 1 `
  if (docs.isDel) dataSql += `AND isDel = ${docs.isDel} `
  if (docs.search) dataSql += `AND (${docs.search}) `
  dataSql += `ORDER BY ${docs.order} LIMIT ${docs.skip}, ${docs.size}`
  const result = await monitorData(dataSql)
  return result
}

const linkCount = async (docs) => {
  let dataSql = `SELECT count(*) AS Count FROM v_link_list WHERE TaskId > 0 `
  if (docs.subid) dataSql += `AND SubscriptionID = ${docs.subid} `
  if (docs.skuid) dataSql += `AND SkuId = ${docs.skuid} `
  // if (docs.bansub) dataSql += `AND SkuId < 1 `
  if (docs.isDel) dataSql += `AND isDel = ${docs.isDel} `
  if (Number.parseInt(docs.isDel)) dataSql += `AND SkuId > 0 `
  if (docs.search) dataSql += `AND (${docs.search}) `
  const result = await monitorData(dataSql)
  return result
}

const infoSet = async (docs) => {
  const sql = `UPDATE assetdata SET isDel = ${docs.status} WHERE AssetID = ${docs.id}`
  await monitorData(sql)

  return docs
}

const skuCheck = async (docs) => {
  let dataSql = `SELECT b.SubscriptionID as subid, a.TaskID as taskid, 
  a.SKUID as skuid FROM skubaseinfo a left join bps_subscript.subscription b 
  ON a.TaskID = b.TaskID WHERE a.isDel = 0 `
  if (docs.subid) dataSql += `AND b.SubscriptionID = ${docs.subid} `
  if (docs.skuid) dataSql += `AND a.SKUID = ${docs.skuid} `
  const result = await monitorData(dataSql)
  return result
}

export default {
  infoList,
  infoCount,
  infoDel,
  linkList,
  linkCount,
  infoSet,
  skuCheck
}