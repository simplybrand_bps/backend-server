import service from './dal'

const infoList = async (docs) => {
  const data = await service.infoList(docs)
  return data
}

const infoCount = async (docs) => {
  const data = await service.infoCount(docs)
  return data
}

const infoDel = async (docs) => {
  const data = await service.infoDel(docs)
  return data
}

const linkList = async (docs) => {
  const data = await service.linkList(docs)
  return data
}

const linkCount = async (docs) => {
  const data = await service.linkCount(docs)
  return data
}

const infoSet = async (docs) => {
  const data = await service.infoSet(docs)
  return data
}

const skuCheck = async (docs) => {
  const data = await service.skuCheck(docs)
  return data
}

export default {
  infoList,
  infoCount,
  infoDel,
  linkList,
  linkCount,
  infoSet,
  skuCheck
}