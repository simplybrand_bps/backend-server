import { sqlquery } from '../../../data'

const monitorData = sqlquery.monitorData

const infoList = async (docs) => {
  let dataSql = `SELECT * FROM dataresult WHERE TaskId = ${docs.subid} `
  if (docs.search) dataSql += `AND (AssetTitle LIKE '%${docs.search}%' OR 
  AssetContent LIKE '%${docs.search}%' OR Shopname LIKE '%${docs.search}%') `
  dataSql += `ORDER BY CheckTime ASC LIMIT ${docs.skip}, ${docs.size}`
  const result = await monitorData(dataSql)
  return result
}

const infoCount = async (docs) => {
  let dataSql = `SELECT count(*) AS Count FROM dataresult WHERE TaskId = ${docs.subid} `
  if (docs.check) dataSql += ` AND CheckTime IS NULL `
  if (docs.search) dataSql += `AND (AssetTitle LIKE '%${docs.search}%' OR 
  AssetContent LIKE '%${docs.search}%' OR Shopname LIKE '%${docs.search}%') `
  const result = await monitorData(dataSql)
  return result
}

const editInfo = async (docs) => {
  let dataSql = 'INSERT INTO assetresult (`AssetID`, `TaskId`, `CheckResult`, `CheckTime`) '
  dataSql += ` VALUES (${docs.assetid}, ${docs.subid}, ${docs.result}, NOW())
    ON DUPLICATE KEY UPDATE CheckResult = ${docs.result}, CheckTime = NOW() `
  const result = await monitorData(dataSql)
  return result
}

const setInfo = async (docs) => {
  let dataSql = 'INSERT INTO assetresult (`AssetID`, `TaskId`,`CheckTime`) '
  dataSql += ` VALUES ${docs.content} ON DUPLICATE KEY UPDATE CheckTime = NOW() `
  const result = await monitorData(dataSql)
  return result
}

const matchList = async (docs) => {
  let dataSql = `SELECT * FROM datamatch WHERE TaskId = ${docs.subid} `
  if (docs.type) dataSql += `AND SkuId = 0 `
  else dataSql += `AND SkuId = ${docs.skuid} `
  if (docs.search) dataSql += `AND (AssetTitle LIKE '%${docs.search}%' OR 
  AssetID LIKE '%${docs.search}%' OR SkuMap LIKE '%${docs.search}%') `
  dataSql += `ORDER BY ${docs.order} LIMIT ${docs.skip}, ${docs.size}`
  const result = await monitorData(dataSql)
  return result
}

const matchCount = async (docs) => {
  let dataSql = `SELECT count(*) AS Count FROM 
  datamatch WHERE TaskId = ${docs.subid} `
  if (docs.type) dataSql += `AND SkuId = 0 `
  else dataSql += `AND SkuId = ${docs.skuid} `
  if (docs.check) dataSql += ` AND CheckTime IS NULL `
  if (docs.search) dataSql += `AND (AssetTitle LIKE '%${docs.search}%' OR 
  AssetID LIKE '%${docs.search}%' OR SkuMap LIKE '%${docs.search}%') `
  const result = await monitorData(dataSql)
  return result
}

const setMatch = async (docs) => {
  let dataSql = 'INSERT INTO assetsku (`AssetID`, `TaskId`, `SkuId`, `CheckTime`) '
  dataSql += ` VALUES (${docs.assetid}, ${docs.subid}, ${docs.skuid}, NOW())
    ON DUPLICATE KEY UPDATE SkuId = ${docs.skuid}, CheckTime = NOW() `
  const result = await monitorData(dataSql)
  return result
}

const checkMatch = async (docs) => {
  let dataSql = 'INSERT INTO assetsku (`AssetID`, `TaskId`, `SkuId`,`CheckTime`) '
  dataSql += ` VALUES ${docs.content} ON DUPLICATE KEY UPDATE CheckTime = NOW() `
  const result = await monitorData(dataSql)
  return result
}

const prematchList = async (docs) => {
  let dataSql = `SELECT * FROM datamatch WHERE TaskId = ${docs.subid} 
  AND isDel = 0 AND SkuId = 0 ${docs.rule} `
  dataSql += `LIMIT ${docs.skip}, ${docs.size}`
  // console.log(dataSql)
  const result = await monitorData(dataSql)
  return result
}

const prematchCount = async (docs) => {
  let dataSql = `SELECT count(*) AS Count FROM datamatch WHERE 
  TaskId = ${docs.subid} AND isDel = 0 AND SkuId = 0 ${docs.rule}`
  const result = await monitorData(dataSql)
  return result
}

export default {
  infoList,
  infoCount,
  editInfo,
  setInfo,
  matchList,
  matchCount,
  setMatch,
  checkMatch,
  prematchList,
  prematchCount
}