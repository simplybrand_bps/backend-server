import service from './dal'

const infoList = async (docs) => {
  const data = await service.infoList(docs)
  return data
}

const infoCount = async (docs) => {
  const data = await service.infoCount(docs)
  return data
}

const setInfo = async (docs) => {
  const data = await service.setInfo(docs)
  return data
}

const editInfo = async (docs) => {
  const data = await service.editInfo(docs)
  return data
}

const matchList = async (docs) => {
  const data = await service.matchList(docs)
  return data
}

const matchCount = async (docs) => {
  const data = await service.matchCount(docs)
  return data
}

const setMatch = async (docs) => {
  const data = await service.setMatch(docs)
  return data
}

const checkMatch = async (docs) => {
  const data = await service.checkMatch(docs)
  return data
}

const prematchList = async (docs) => {
  const data = await service.prematchList(docs)
  return data
}

const prematchCount = async (docs) => {
  const data = await service.prematchCount(docs)
  return data
}

export default {
  infoList,
  infoCount,
  setInfo,
  editInfo,
  matchCount,
  matchList,
  setMatch,
  checkMatch,
  prematchList,
  prematchCount
}