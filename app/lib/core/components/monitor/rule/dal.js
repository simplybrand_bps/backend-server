import { sqlquery } from '../../../data'

const monitorData = sqlquery.monitorData

const addInfo = async (docs) => {
  let dataSql = 'INSERT INTO assetrule (taskid, skuid, rulecontent, rulesql) VALUES '
  dataSql += `( ${docs.subid},${docs.skuid},'${docs.data}','${docs.sql}')`
  await monitorData(dataSql)

  return docs
}

const editInfo = async (docs) => {
  const dataSql = `UPDATE assetrule SET rulecontent = '${docs.data}', rulesql = '${docs.sql}' WHERE ruleid = ${docs.ruleid} `
  await monitorData(dataSql)

  return docs
}

const delInfo = async (docs) => {
  let dataSql = 'UPDATE assetrule SET `status` = 0 '
  dataSql += `WHERE ruleid = ${docs.ruleid}`
  await monitorData(dataSql)

  return docs
}

const viewinfo = async (docs) => {
  let dataSql = `SELECT * FROM (SELECT * FROM assetrule 
    WHERE taskid = ${docs.subid} AND skuid = ${docs.skuid}`
    dataSql += ' ) a HAVING `status` = 1'
  const result = await monitorData(dataSql)

  return result
}

const viewstatus = async (docs) => {
  let dataSql = `SELECT * FROM rulestatus WHERE skuid = ${docs.skuid} `
  if (docs.ruleid) dataSql += ` AND ruleid = ${docs.ruleid} `
  dataSql += `ORDER BY id DESC LIMIT 1`
  const result = await monitorData(dataSql)

  return result
}

const setstatus = async (docs) => {
  const dataSql = `INSERT INTO rulestatus (ruleid,skuid) VALUES (${docs.ruleid},${docs.skuid}) `
  
  const result = await monitorData(dataSql)

  return result
}

const getDataId = async (docs) => {
  const dataSql = `SELECT AssetID FROM datamatch WHERE TaskId = ${docs.subid} 
  AND isDel = 0 AND SkuId = 0 ${docs.sqlcontent} `
  
  const result = await monitorData(dataSql)

  return result
}

const setFinish = async (docs) => {
  let dataSql = 'INSERT INTO rulestatus (ruleid,skuid,`status`) VALUES '
  dataSql += `(${docs.ruleid},${docs.skuid},2) `
  
  const result = await monitorData(dataSql)

  return result
}

const ruleinfo = async (docs) => {
  let dataSql = `SELECT * FROM (SELECT * FROM assetrule 
    WHERE ruleid = ${docs.ruleid} AND skuid = ${docs.skuid}`
    dataSql += ' ) a HAVING `status` = 1'
  const result = await monitorData(dataSql)

  return result
}

const checkMatch = async (docs) => {
  let dataSql = 'INSERT INTO assetsku (`AssetID`, `TaskId`, `SkuId`) '
  dataSql += ` VALUES ${docs.content} ON DUPLICATE KEY UPDATE SkuId = ${docs.skuid} `
  const result = await monitorData(dataSql)
  return result
}


export default {
  addInfo,
  editInfo,
  delInfo,
  viewinfo,
  viewstatus,
  setstatus,
  getDataId,
  setFinish,
  ruleinfo,
  checkMatch
}