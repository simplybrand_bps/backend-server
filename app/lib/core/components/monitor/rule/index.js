import service from './dal'
import fetch from 'node-fetch'


const addInfo = async (docs) => {
  const data = await service.addInfo(docs)
  return data
}

const editInfo = async (docs) => {
  const data = await service.editInfo(docs)
  return data
}

const delInfo = async (docs) => {
  const data = await service.delInfo(docs)
  return data
}

const viewInfo = async (docs) => {
  const data = await service.viewinfo(docs)
  return data
}

const viewstatus = async (docs) => {
  const data = await service.viewstatus(docs)
  return data
}

const setstatus = async (docs) => {
  // const remoteIp = await serverIP()
  const remoteIp = '106.15.35.56'
  const remoteStatus = await serverStatus(remoteIp, docs.ruleid)
  if (remoteStatus) {
    const parseResult = JSON.parse(remoteStatus)
    if(parseResult.result === 'successful') await service.setstatus(docs)
  } else throw new Error('Error happens with the status code: 504, the key is: SMS')
  return docs
}


const serverIP = async () => {
  return new Promise((resolve, reject) => {
    fetch('http://firewall.simplybrand.io/Index.aspx?action=get&name=simply', 
    { credentials: 'include', headers: { 'Content-Type': 'text/plain; charset=UTF-8', Connection: 'keep-alive' } }
    ).then(res => {
      if (res.ok) return res.text()
      else throw new Error('Error happens with the status code: 504, the key is: SMS')
    }).then(resbody => {
      resolve(resbody)
    }).catch(err => reject(err))
  })
}

const serverStatus = async (remoteIp,ruleid) => {
  return new Promise((resolve, reject) => {
    fetch(`http://${remoteIp}:8000/onerule`,{
      method:'POST',
      headers:{
         'Content-Type': 'application/json'
      },
      body:JSON.stringify({
        ruleid
      })
    })
    .then(res => {
      if (res.ok) return res.text()
      else throw new Error('Error happens with the status code: 504, the key is: SMS')
    }).then(resbody => {
      resolve(resbody)
    }).catch(err => reject(err))
  })
}

const getDataId = async (docs) => {
  const data = await service.getDataId(docs)
  return data
}

const setFinish = async (docs) => {
  const data = await service.setFinish(docs)
  return data
}

const ruleinfo = async (docs) => {
  const data = await service.ruleinfo(docs)
  return data
}

const checkMatch = async (docs) => {
  const data = await service.checkMatch(docs)
  return data
}

export default {
  addInfo,
  editInfo,
  delInfo,
  viewInfo,
  viewstatus,
  setstatus,
  getDataId,
  setFinish,
  ruleinfo,
  checkMatch
}