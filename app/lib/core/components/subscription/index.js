/**
 * 配置可用模块
 */
import info from './info'
import policy from './policy'

export default {
  info,
  policy
}