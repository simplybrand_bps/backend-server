import { sqlquery } from '../../../data'
import { support } from '../../../utils'

const baseData = sqlquery.baseData
const monitorData = sqlquery.monitorData

const addInfo = async (docs) => {
  let dataSql = 'INSERT INTO `task`(`TaskName`, `BrandID`, `ChannelIDList`, `status`) VALUES '
  dataSql += `('${docs.name}', '${docs.brandid}',  '${docs.channel}', 1)`

  const dataResult = await monitorData(dataSql)

  let baseSql = 'INSERT INTO `subscription`(`SubscriptionName`, `mainId`, `PackageID`, `init`, `expire`, `TaskID`,`status`, userid) VALUES '
  baseSql += `('${docs.name}', '${docs.mainid}', '${docs.packageid}', '${docs.init}', '${docs.expire}', '${dataResult.insertId}', 1, '${docs.userid}')`
  const baseResult = await baseData(baseSql)

  let extSql = 'INSERT INTO `contract`(`brandid`, `poid`, `taskid`) VALUES '
  extSql += `('${docs.brandid}', '${baseResult.insertId}', '${dataResult.insertId}')`

  await monitorData(extSql)

  support.email.notify(dataResult.insertId)

  return docs
}

const editInfo = async (docs) => {
  let dataSql = 'UPDATE task SET `TaskName` = '
  dataSql += `'${docs.name}', ChannelIDList = '${docs.channel}' WHERE TaskID = ${docs.subid}`
  await monitorData(dataSql)

  return docs
}

const setInfo = async (docs) => {
  const dataSql = `UPDATE task SET filter_txt = '${docs.filter}', keep_txt = '${docs.keep}' WHERE TaskID = ${docs.subid}`
  await monitorData(dataSql)

  return docs
}

const viewInfo = async (docs) => {
  let dataSql = 'SELECT a.*, b.brand, b.`name` AS brandName, b.keywords, '
  dataSql += `c.MatchCount, c.UndoCount as UnMatch FROM task a 
  LEFT JOIN brandlist b ON a.brandId = b.brandId 
  LEFT JOIN matchresult c ON a.TaskID = c.TaskId
  WHERE a.TaskID = ${docs.subid}`
  const cbData = await monitorData(dataSql)

  return cbData
}

export default {
  addInfo,
  editInfo,
  setInfo,
  viewInfo
}