import service from './dal'

const viewInfo = async (docs) => {
  const data = await service.viewInfo(docs)
  return data
}

const addInfo = async (docs) => {
  const data = await service.addInfo(docs)
  return data
}

const editInfo = async (docs) => {
  const data = await service.editInfo(docs)
  return data
}

const setInfo = async (docs) => {
  const data = await service.setInfo(docs)
  return data
}

export default {
  viewInfo,
  addInfo,
  editInfo,
  setInfo
}