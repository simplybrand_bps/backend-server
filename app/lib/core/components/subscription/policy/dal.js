import { sqlquery } from '../../../data'
import { support } from '../../../utils'

const baseData = sqlquery.baseData
const monitorData = sqlquery.monitorData

const infoList = async (docs) => {
  let dataSql = `SELECT * FROM subtask `
  let searchStr =  'WHERE `name` LIKE '
  searchStr += `'%${docs.search}%' OR 
  email LIKE '%${docs.search}%' `
  if (docs.search) dataSql += searchStr
  dataSql += ` LIMIT ${docs.skip}, ${docs.size}`
  const result = await baseData(dataSql)
  return result
}

const infoCount = async (docs) => {
  let dataSql = `SELECT  count(*) AS Count FROM subtask `
  let searchStr =  'WHERE `name` LIKE '
  searchStr += `'%${docs.search}%' OR 
  email LIKE '%${docs.search}%' `
  if (docs.search) dataSql += searchStr
  const result = await baseData(dataSql)

  return result
}

const addInfo = async (docs) => {
  const sql = `SELECT * FROM subscription WHERE SubscriptionID = ${docs.poid} LIMIT 1`
  const result = await baseData(sql)
  if (result.length) {
    const doc = result[0]

    let dataSql = 'INSERT INTO `subscription`(`SubscriptionName`, `mainId`, `init`, `expire`, `PackageID`,`TaskID`, `status`, userid) VALUES '
    dataSql += `('${doc.SubscriptionName}', '${doc.mainId}', '${docs.init}', '${docs.expire}', '${doc.PackageID}','${doc.TaskID}', '${doc.status}')`
    await baseData(dataSql)
  }

  return result
}

const editInfo = async (docs) => {
  const dataSql = `UPDATE subscription SET init = '${docs.init}', expire = '${docs.expire}', 
  mainId = '${docs.mainid}', userid = '${docs.userid}',
  SubscriptionName = '${docs.name}', PackageID = '${docs.packageid}' WHERE SubscriptionID = ${docs.poid}`
  await baseData(dataSql)

  return docs
}

const delInfo = async (docs) => {
  let dataSql = 'UPDATE subscription SET `status` = '
  dataSql += `'${docs.status}' WHERE SubscriptionID = ${docs.poid}`

  await baseData(dataSql)

  const sql = `SELECT * FROM subscription WHERE SubscriptionID = ${docs.poid}`
  const result = await baseData(sql)

  if (result.length && result[0].TaskID) {
    let sqlcheck = 'SELECT * FROM subscription WHERE `status` = 1 AND'
    sqlcheck += ` TaskID = ${result[0].TaskID} `
    const retCheck = await baseData(sql)
    //所有关联此任务的订阅都被禁用时，停止此任务
    if (!retCheck.length) {
      let tsksql = 'UPDATE task SET `status` = 0 '
      tsksql += ` WHERE TaskID = ${result[0].TaskID}`
      await monitorData(tsksql)
    } else if (retCheck.length === 1) {
      support.email.notify(result[0].TaskID)
    }
  }
  return result
}

export default {
  infoList,
  infoCount,
  addInfo,
  editInfo,
  delInfo,
  editInfo
}
