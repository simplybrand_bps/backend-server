import service from './dal'

const addInfo = async (docs) => {
  const data = await service.addInfo(docs)
  return data
}

const editInfo = async (docs) => {
  const data = await service.editInfo(docs)
  return data
}

const infoList = async (docs) => {
  const data = await service.infoList(docs)
  return data
}

const infoCount = async (docs) => {
  const data = await service.infoCount(docs)
  return data
}

const delInfo = async (docs) => {
  const data = await service.delInfo(docs)
  return data
}

export default {
  infoList,
  infoCount,
  addInfo,
  editInfo,
  delInfo,
  editInfo
}