import { sqlquery } from '../../../data'

const monitorData = sqlquery.monitorData

const pluginadd = async (BrandName,insertId) => {
  let dataSql = 'INSERT INTO `anti_counterfeiting_test`.`BrandList` (BrandName, posubid) '
  dataSql += `VALUES ('${BrandName}', '${insertId}')`
  const result = await monitorData(dataSql)

  return result
}

const infoList = async (docs) => {
  let dataSql = `SELECT a.*, IFNULL(b.conum,0) AS count FROM brandlist a
  LEFT JOIN (SELECT brandid, COUNT(id) AS conum from contract 
  GROUP BY brandid) b ON a.brandId = b.brandid `
  let searchStr =  'WHERE a.`name` LIKE '
  searchStr += `'%${docs.search}%' OR a.brand LIKE '%${docs.search}%' `
  if (docs.search) dataSql += searchStr
  if (docs.type)  dataSql += `HAVING count < 1 `
  if (docs.size) dataSql += `LIMIT ${docs.skip}, ${docs.size}`
  const result = await monitorData(dataSql)
  return result
}

const getInfo = async (docs) => {
  const dataSql = 'SELECT * FROM brandlist WHERE `name` = ' + `'${docs.search}'`
  const result = await monitorData(dataSql)
  return result
}

const infoCount = async (docs) => {
  let dataSql = `SELECT count(*) AS Count  FROM brandlist `
  let searchStr =  'WHERE `name` LIKE '
  searchStr += `'%${docs.search}%' OR 
  brand LIKE '%${docs.search}%' `
  if (docs.search) dataSql += searchStr
  const result = await monitorData(dataSql)

  return result
}

const addInfo = async (docs) => {
  let dataSql = 'INSERT INTO brandlist (brand, `name`,'
  dataSql += `trademark, keywords, logo) VALUES (
  '${docs.brand}', '${docs.name}', '${docs.trademark}', 
  '${docs.keywords}', '${docs.logo}')`
  const result = await monitorData(dataSql)

  /**
   * plugin
   */
  await pluginadd(docs.brand, result.insertId)
  /**
   * plugin
   */ 

  return result
}

const editInfo = async (docs) => {
  let dataSql = 'UPDATE brandlist SET `name` = '
  dataSql += `'${docs.name}', brand = '${docs.brand}',
  trademark = '${docs.trademark}', keywords = '${docs.keywords}', 
  logo = '${docs.logo}'  WHERE brandId = ${docs.id}`
  await monitorData(dataSql)

  return docs
}

const delInfo = async (docs) => {
  let dataSql = 'UPDATE brandlist SET `status` = '
  dataSql += `'${docs.status}' WHERE brandId = ${docs.id}`

  await monitorData(dataSql)

  return docs
}

export default {
  getInfo,
  infoList,
  infoCount,
  addInfo,
  editInfo,
  delInfo
}