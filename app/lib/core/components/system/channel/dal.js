import { sqlquery } from '../../../data'

const monitorData = sqlquery.monitorData

const infoList = async (docs) => {
  let dataSql = `SELECT * FROM channel `
  let searchStr =  'WHERE `ChannelName` LIKE '
  searchStr += `'%${docs.search}%' `
  if (docs.search) dataSql += searchStr
  if (docs.size) dataSql += `LIMIT ${docs.skip}, ${docs.size}`
  const result = await monitorData(dataSql)
  return result
}

const infoCount = async (docs) => {
  let dataSql = `SELECT count(*) AS Count  FROM channel `
  let searchStr =  'WHERE `ChannelName` LIKE '
  searchStr += `'%${docs.search}%' `
  if (docs.search) dataSql += searchStr
  const result = await monitorData(dataSql)

  return result
}

export default {
  infoList,
  infoCount
}