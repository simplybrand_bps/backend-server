import service from './dal'

const infoList = async (docs) => {
  const data = await service.infoList(docs)
  return data
}

const infoCount = async (docs) => {
  const data = await service.infoCount(docs)
  return data
}

export default {
    infoList,
    infoCount
}