import { sqlquery } from '../../../data'

const monitorData = sqlquery.monitorData

const infofilter = async () => {
  const dataSql = `SELECT  date_format(createdAt,'%Y-%m-%d') AS dateString 
    FROM imagecheck GROUP BY dateString ORDER BY dateString DESC `
  const result = await monitorData(dataSql)
  return result
}

const infoList = async (docs) => {
  let dataSql = `SELECT * FROM imagecheck WHERE id > 0 `
  if (docs.datatime) dataSql += `AND createdAt BETWEEN "${docs.datatime} 00:00:00" AND "${docs.datatime} 23:59:59" `
  if (docs.checkNu) dataSql += `AND result IS NULL `
  if (docs.checkNo) dataSql += `AND result IS NOT NULL `
  dataSql += `ORDER BY createdAt DESC, checktime ASC `
  if (docs.size) dataSql += `LIMIT ${docs.skip}, ${docs.size}`
  const result = await monitorData(dataSql)
  return result
}

const infoCount = async (docs) => {
  let dataSql = `SELECT count(*) AS Count  FROM imagecheck WHERE id > 0 `
  if (docs.datetime) dataSql += `AND createdAt BETWEEN "${docs.datetime} 00:00:00" AND "${docs.datetime} 23:59:59" `
  if (docs.checkNu) dataSql += `AND result IS NULL `
  if (docs.checkNo) dataSql += `AND result IS NOT NULL `
  const result = await monitorData(dataSql)

  return result
}

const setstatus = async (docs) => {
  let dataSql = `UPDATE imagecheck SET result = '${docs.status}', 
  checktime = now() WHERE id = ${docs.id}`
  const result = await monitorData(dataSql)

  return result
}

const listfilter = async () => {
  let dataSql = `SELECT date_format(createdAt,"%Y-%m-%d") AS CurrentDate 
    FROM imagecheck WHERE result IS NOT `
  dataSql += `NULL GROUP BY CurrentDate ORDER BY CurrentDate DESC`
  const result = await monitorData(dataSql)
  return result
}

const listdata = async (docs) => {
  let dataSql = `SELECT a.AssetImg, a.CurrentDate, a.DataImg, b.*, 
  f.AssetTitle AS DataTitle, f.AssetURL AS DataURL,
  d.ShopName, d.SellerName, d.ShopUrl, e.ChannelName FROM image_checkout a 
  LEFT JOIN assetdata f ON a.DataId = f.AssetID
  LEFT JOIN assetdata b ON a.Assetid = b.AssetID 
  LEFT JOIN bps_package.asssetshop c ON a.Assetid = c.AssetID
  LEFT JOIN shopinfo d ON c.ShopId = d.ShopId
  LEFT JOIN channel e ON b.ChannelId = e.ChannelId `
  if(docs.docdate) dataSql += `WHERE a.CurrentDate = "${docs.docdate}" `
  if(docs.start) dataSql += `WHERE a.CurrentDate between "${docs.start}" and "${docs.end}" `
  const result = await monitorData(dataSql)
  return result
}

export default {
  infofilter,
  infoList,
  infoCount,
  setstatus,
  listfilter,
  listdata
}