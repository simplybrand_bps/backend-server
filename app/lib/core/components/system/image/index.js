import service from './dal'

const infofilter = async (docs) => {
  const data = await service.infofilter(docs)
  return data
}

const infoList = async (docs) => {
  const data = await service.infoList(docs)
  return data
}

const infoCount = async (docs) => {
  const data = await service.infoCount(docs)
  return data
}

const setstatus = async (docs) => {
  const data = await service.setstatus(docs)
  return data
}

const listfilter = async (docs) => {
  const data = await service.listfilter(docs)
  return data
}

const listdata = async (docs) => {
  if (!docs.docdate & !docs.start) {
    const datecheck = await service.listfilter(docs)
    docs.docdate = datecheck.length ? datecheck[0].CurrentDate : 0
  }
  const data = await service.listdata(docs)
  return data
}

export default {
  infofilter,
  infoList,
  infoCount,
  setstatus,
  listfilter,
  listdata
}