/**
 * 配置可用模块
 */
import brand from './brand'
import channel from './channel'
import level from './level'
import image from './image'

export default {
    brand,
    channel,
    level,
    image
}