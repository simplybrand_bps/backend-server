import { sqlquery } from '../../../data'

const baseData = sqlquery.baseData

const infoList = async (docs) => {
  let dataSql = `SELECT * FROM level `
  let searchStr =  'WHERE `desc` LIKE '
  searchStr += `'%${docs.search}%' `
  if (docs.search) dataSql += searchStr
  if (docs.size) dataSql += `LIMIT ${docs.skip}, ${docs.size}`
  const result = await baseData(dataSql)
  return result
}

const infoCount = async (docs) => {
  let dataSql = `SELECT count(*) AS Count  FROM level `
  let searchStr =  'WHERE `desc` LIKE '
  searchStr += `'%${docs.search}%' `
  if (docs.search) dataSql += searchStr
  const result = await baseData(dataSql)

  return result
}

export default {
  infoList,
  infoCount
}