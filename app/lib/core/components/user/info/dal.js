import { sqlquery } from '../../../data'

const userData = sqlquery.userData

const addAccount = async (docs, info) => {
  const query = {
    account: `('${docs.email}', 'active')`
  }
  const accountSql = 'INSERT INTO `account`(`email`, `status`) VALUES'
  const accountResult = await userData(`${accountSql} ${query.account}`)

  query.baseinfo = `('${docs.username}', '${docs.phone}', '${docs.company}', '${docs.url}', '${accountResult.insertId}')`
  query.password = `('${info.password}', '${info.salt}', '${accountResult.insertId}')`

  const userinfoSql = 'INSERT INTO `baseinfo`(`username`, `telephone`, `company`, `url`, `userId`) VALUES'
  const passwordSql = 'INSERT INTO `password`(`password`, `salt`, `userId`) VALUES'
  const dataSql = `UPDATE account SET mainId = ${accountResult.insertId} WHERE userId = ${accountResult.insertId}`

  await userData(`${userinfoSql} ${query.baseinfo}`)
  await userData(`${passwordSql} ${query.password}`)
  await userData(dataSql)

  return docs
}

const editAcoout = async (docs) => {
  const dataSql = `UPDATE account SET email = '${docs.email}' WHERE userId = ${docs.id}`
  await userData(dataSql)

  return docs
}

const setAcoout = async (docs) => {
  let dataSql = 'UPDATE account SET `status` = '
  dataSql += `'${docs.setting}' WHERE userId = ${docs.id}`

  await userData(dataSql)

  return docs
}

const editInfo = async (docs) => {
  const dataSql = `UPDATE baseinfo SET ${docs.content} WHERE userId = ${docs.id}`
  await userData(dataSql)

  return docs
}

const editPassword = async (docs) => {
  const dataSql = `UPDATE password SET password = '${docs.password}', salt = '${docs.salt}' WHERE userId = ${docs.id}`
  await userData(dataSql)

  return docs
}

const infoList = async (docs) => {
  let dataSql = `SELECT  b.userId, a.email, a.mainId,
  b.username, b.company, b.telephone AS phone,b.url, 
  a.createdAt, c.createAt AS loginTime, `
  dataSql += ' a.`status` FROM account a '
  dataSql += `LEFT JOIN baseinfo b ON a.userId = b.userId
  LEFT JOIN login c ON a.userId = c.userId `
  const searchStr =  `WHERE a.email LIKE 
  '%${docs.search}%' OR b.company LIKE '%${docs.search}%' `
  if (docs.search) dataSql += searchStr
  dataSql += 'ORDER BY a.userId DESC '
  if (docs.size) dataSql += `LIMIT ${docs.skip}, ${docs.size}`
  const result = await userData(dataSql)

  return result
}

const infoCount = async (docs) => {
  let dataSql = `SELECT count(*) AS Count FROM account a 
  LEFT JOIN baseinfo b ON a.userId = b.userId `
  const searchStr =  `WHERE a.email LIKE 
  '%${docs.search}%' OR b.company LIKE '%${docs.search}%' `
  if (docs.search) dataSql += searchStr
  const result = await userData(dataSql)

  return result
}

const checkAccount = async (docs) => {
  let dataSql = 'SELECT userId, email, mainId, '
  dataSql += 'createdAt, `status` FROM account WHERE '
  if (docs.type) dataSql += ` email LIKE '%${docs.email}%'`
  else dataSql += ` email = '${docs.email}'`
  const result = await userData(dataSql)

  return result
}

export default {
  addAccount,
  editAcoout,
  setAcoout,
  editInfo,
  editPassword,
  infoList,
  infoCount,
  checkAccount
}
