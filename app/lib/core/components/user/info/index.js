import service from './dal'

const addAccount = async (docs, info) => {
  const data = await service.addAccount(docs, info)
  return data
}

const editAcoout = async (docs) => {
  const data = await service.editAcoout(docs)
  return data
}

const editInfo = async (docs) => {
  let doctext = ''
  doctext += `username = "${docs.username}",`
  doctext += `company = "${docs.company}",`
  doctext += `telephone = "${docs.phone}",`
  doctext += `url = "${docs.url}",`
  if (doctext.length) {
    docs.content = doctext.substring(0,doctext.length-1)
    await service.editInfo(docs)
  }
  return docs
}

const editPassword = async (docs) => {
  const data = await service.editPassword(docs)
  return data
}

const infoList = async (docs) => {
  const data = await service.infoList(docs)
  return data
}

const infoCount = async (docs) => {
  const data = await service.infoCount(docs)
  return data
}

const checkAccount = async (docs) => {
  const data = await service.checkAccount(docs)
  return data
}

const setAccount = async (docs) => {
  const data = await service.setAcoout(docs)
  return data
}

export default {
  addAccount,
  editAcoout,
  setAccount,
  editInfo,
  editPassword,
  infoList,
  infoCount,
  checkAccount
}
