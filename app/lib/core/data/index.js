'use strict'
/**
 * 模块访问的入口
 */
import dbModels from './models'
import { dbquery, sqlquery } from './query'
const localDB = {
  db: dbModels,
  dbquery
}

export {
  localDB,
  sqlquery,
}
