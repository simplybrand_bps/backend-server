
import db from '../query/sequelize'
/**
 * 注册可用数据库
 */
const defaultModels = {
  userModels: {
    Account: 'user/account',
    Password: 'user/password',
    ACL: 'user/acl'
  }
}

const dbModels = db(defaultModels.userModels)

export default dbModels
