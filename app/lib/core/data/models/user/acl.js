'use strict'
/**
 * charset and collate are set at database level.
 * "charset": "utf8mb4"
 * "collate": "utf8mb4_unicode_520_ci"
 */
export default (sequelize, DataTypes) => {
  return sequelize.define('acl', {
    id: { autoIncrement: true, primaryKey: true, type: DataTypes.INTEGER.UNSIGNED },
    uuid: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
      notEmpty: true,
      validate: { isUUID: 4 },
      unique: true
    },
    account: {
      type: DataTypes.STRING(120),
      allowNull: false,
      notEmpty: true,
      unique: true
    },
    userId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: true },
    expire: { type: DataTypes.INTEGER.UNSIGNED, defaultValue: DataTypes.NOW },
    createAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
    invalidAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
    status: { type: DataTypes.ENUM, values: ['active', 'expired'], defaultValue: 'active' }
  })
}
