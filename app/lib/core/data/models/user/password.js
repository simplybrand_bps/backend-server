'use strict'
/**
 * charset and collate are set at database level.
 * "charset": "utf8mb4"
 * "collate": "utf8mb4_unicode_520_ci"
 */
export default (sequelize, DataTypes) => {
  return sequelize.define(
    'password',
    {
      id: { autoIncrement: true, primaryKey: true, type: DataTypes.INTEGER.UNSIGNED },
      userId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
      password: {
        type: DataTypes.STRING(128),
        allowNull: false,
        notEmpty: true
      },
      salt: { type: DataTypes.STRING(128), allowNull: false }
    },
    {
      timestamps: false,
      freezeTableName: true
    }
  )
}
