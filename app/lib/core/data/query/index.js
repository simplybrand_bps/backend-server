import dbquery from './sequelize/methods'
import sqlquery from './mysql'

export {
  dbquery,
  sqlquery
}
