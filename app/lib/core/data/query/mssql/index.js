import localClient from 'mssql'

const config = {
    user: 'sa',
    password: 'simplybrand@123',
    server: '180.175.25.112,5007',
    database: 'Commerce',
    options: {
      encrypt: false,
      enableArithAbort: false
    }
}

const logger = global.logger

const localData = (sql) => {
  logger.silly(sql)
  return new Promise((resolve, reject) => {
    localClient.on('error', err => {
      reject(err)
    })
     
    localClient.connect(config).then(pool => {
      return pool.request().query(sql)
    }).then(results => {
      resolve(results)
    }).catch(err => {
      reject(err)
    });
  })
}

export default {
  localData
}
