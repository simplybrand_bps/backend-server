import userClient from './client'
import baseClient from './base'
import dataClient from './digital'
import priceClient from './monitor'
import extClient from './project'
import monitorClient from './data'

const logger = global.logger

const userData = (sql) => {
  logger.silly(sql)
  return new Promise((resolve, reject) => {
    userClient.query(sql, (err, results) => {
      if (err) reject(err)
      resolve(results)
    })
  })
}

const baseData = (sql) => {
  logger.silly(sql)
  return new Promise((resolve, reject) => {
    baseClient.query(sql, (err, results) => {
      if (err) reject(err)
      resolve(results)
    })
  })
}

const viewData = (sql) => {
  logger.silly(sql)
  return new Promise((resolve, reject) => {
    dataClient.query(sql, (err, results) => {
      if (err) reject(err)
      resolve(results)
    })
  })
}

const priceData = (sql) => {
  logger.silly(sql)
  return new Promise((resolve, reject) => {
    priceClient.query(sql, (err, results) => {
      if (err) reject(err)
      resolve(results)
    })
  })
}


const extData = (sql) => {
  logger.silly(sql)
  return new Promise((resolve, reject) => {
    extClient.query(sql, (err, results) => {
      if (err) reject(err)
      resolve(results)
    })
  })
}

const monitorData = (sql) => {
  logger.silly(sql)
  return new Promise((resolve, reject) => {
    monitorClient.query(sql, (err, results) => {
      if (err) reject(err)
      resolve(results)
    })
  })
}

export default {
  userData,
  baseData,
  viewData,
  priceData,
  extData,
  monitorData
}
