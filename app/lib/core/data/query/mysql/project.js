import mysql from 'mysql2'
import nconf from 'nconf'

const sqlconfig = nconf.get('plugin')
const mysqlConnPool = mysql.createPool({
  host: sqlconfig.host,
  port: sqlconfig.port,
  user: sqlconfig.username,
  password: sqlconfig.password,
  database: sqlconfig.database,
  waitForConnections: sqlconfig.waitForConnections,
  connectionLimit: sqlconfig.connectionLimit,
  queueLimit: sqlconfig.queueLimit
})
export default mysqlConnPool