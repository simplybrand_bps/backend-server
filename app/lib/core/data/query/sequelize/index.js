'use strict'
import path from 'path'
import sequelize from './setup'
import nconf from 'nconf'
nconf.file('secretKey', path.join(__dirname, `../../../../../../.env/config.${process.env.NODE_ENV}.json`))

export default async (defaultModels) => {

  const logger = global.logger
  try {
    const db = await sequelize()
    const models = {}
    for (const modelFile in defaultModels) {
      const filepsth = path.join(__dirname, `../../models/${defaultModels[modelFile]}.js`)
      logger.debug(filepsth)
      const model = await db.import(filepsth)
      // model.sequelize.sync()
      models[modelFile] = model
    }
    models.db = db
    return models
  } catch (e) {
    logger.warn(
      `Something went wrong with Database, not in sync, can not continue\n
      ${e.stack}`
    )
  }
}
