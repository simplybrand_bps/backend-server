'use strict'
import nconf from 'nconf'
import Sequelize from 'sequelize'

export default async () => {
  const logger = global.logger
  try {
    const sqlconfig = await nconf.get('admin')
    sqlconfig.logging = msg => logger.silly(msg)
    const db = new Sequelize(sqlconfig.database, sqlconfig.username, sqlconfig.password, sqlconfig)
    return db
  } catch (e) {
    logger.error(
      ` Crashed!\n
        Invalid authentication type detected: 
        Please make sure mysql settings exist in config/config.${process.env.NODE_ENV}.json
      `)
    process.exit(-11)
  }
}
