'use strict'
/**
 * 模块访问的入口
 */
import express from 'express'
import service from './router'

const router = express.Router()

for (const api in service) {
  router.use(service[api])
}

export default router
