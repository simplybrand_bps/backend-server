'use strict'
import service from '../../components'

const baseInfo = async (docs) => {
  const tokenInfo = await service.identify.acl.checkJWT(docs.token)
  if (!tokenInfo) throw new Error('Error happens with the status code: 401, the key is: Token')
  return tokenInfo
}

const authInfo = async (docs) => {
  const tokenInfo = await service.identify.acl.checkJWT(docs.token)
  if (!tokenInfo || !tokenInfo.mainid) throw new Error('Error happens with the status code: 401, the key is: Token')
  // memberId即policy的ID
  const memberInfo = await service.right.policy.getByBoth({ mainid: tokenInfo.mainid, type: docs.status })
  // 返回email、userid、mainid、memberid以及所属模块type

  if (memberInfo) return Object.assign(tokenInfo, memberInfo)
  else throw Error('Error happens with the status code: 403, the key is: Token')
}

const pageInfo = async (docs) => {
  const tokenInfo = await service.identify.acl.checkJWT(docs.token)
  if (!tokenInfo || !tokenInfo.mainid) throw new Error('Error happens with the status code: 401, the key is: Token')
  // 获取memberid
  const memberInfo = await service.right.policy.getByBoth({ mainid: tokenInfo.mainid, type: docs.status })
  if (!memberInfo) throw Error('Error happens with the status code: 403, the key is: Token')
  // 由memberid获取用户类型，目前有1计点扣费和2包年包月两种
  const authInfo = await service.right.authority.getByBoth(memberInfo)
  let defaultType = 0
  if (!authInfo.length) throw Error('Error happens with the status code: 403, the key is: Token')
  authInfo.forEach(element => {
    defaultType = element.type > defaultType ? element.type : defaultType
  })
  // 只要有2，则跳过余额判定
  if (defaultType < 2) {
    // Todo 查询余额
    const memberQuota = await service.view.balance.getQuota(tokenInfo.mainid)
    if (!memberQuota || memberQuota.balance <= 0) throw Error('Error happens with the status code: 403, the key is: balance')
  }
  return Object.assign(tokenInfo, memberInfo)
}

export default {
  baseInfo,
  pageInfo,
  authInfo
}
