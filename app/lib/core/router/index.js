/**
 * 配置可用模块
 */
import landing from './landing'
import manage from './manage'
import system from './system'

/**
 * ToDo
 * 建立目录，直观表现每个模块注册的路由
 */

const routers = Object.assign(landing,manage,system)

export default routers
