/**
 * 数据分发，仅在本文件中进行与其他模块的业务连通
 */
'use strict'
import moment from 'moment'
import service from '../../../components'

const login = async (docs) => {
  // 查找账号
  const accountInfo = await service.identify.account.checkAccount(docs.account)
  // 如果不存在，则查看待激活记录
  if (!accountInfo)  throw new Error(`Error happens with the status code: 400, the key is: ${docs.account}`)

  // 验证密码
  const pwdInfo = await service.identify.password.checkPwd({ userid: accountInfo.userid, password: docs.password })
  if (!pwdInfo) throw new Error(`Error happens with the status code: 400, the key is: ${docs.password}`)

  const query = {
    ip: docs.ip,
    account: docs.account,
    userid: accountInfo.userid
  }
  // 登录
  const tokenInfo = await service.identify.acl.createJWT(query)

  return {
    success: true,
    code: 0,
    token: tokenInfo.token
  }
}

const logout = async (submitted) => {
  return submitted
}

export default {
  login,
  logout
}
