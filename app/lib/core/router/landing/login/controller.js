/**
 * controllers 中负责与 http 的连接工作
 * 只做简单参数的验证，传参和调起 services
 * 禁止把 rep, res 对像做为参数传递到 services 层
 */
import serverErr from './errors'
import service from './assembly'

const Validater = {
  account: (value) => {
    if (typeof (value) !== 'string') return false
    return true
  },
  password: (value) => {
    if (typeof (value) !== 'string') return false
    return true
  }
}
const paramsFormat = (bodyParams) => {
  const paramHandler = {
    get: (target, key) => {
      if (Validater[key]) {
        if (Validater[key](target[key])) return target[key]
        else throw new Error(`Error happens with the status code: 400, the key is: ${key}`)
      }
    }
  }
  const bodyParse = new Proxy(bodyParams, paramHandler)
  return bodyParse
}
// 登录
const login = async (req, res) => {
  try {
    /**
     * post方法，读取req.body中的参数
     * @param {string} account
     * @param {string} password
     */
    const bodyParams = req.body
    const bodyParse = paramsFormat(bodyParams)

    const params = ((
      {
        account,
        password
      }) => (
      {
        account,
        password,
        ip: req.headers['x-forwarded-for'] || req.ip || req._remoteAddress || (req.connection && req.connection.remoteAddress) || undefined
      }
    ))(bodyParse)
    const serviceResult = await service.login(params)
    return res.json(serviceResult)
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}
// 登出
const logout = async (req, res) => {
  try {
    /**
     * get方法，读取req.headers中的Authorization
     * @param {string} Authorization
     */
    return res.json({
      success: true,
      message: 'OK'
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

export default {
  login,
  logout
}
