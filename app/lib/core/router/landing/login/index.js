/**
 * 模块访问的入口
 * 该模块不进行任何逻辑处理
 */
'use strict'
import express from 'express'
import controller from './controller'
const baseUri = '/sys'

const router = express.Router()

router.post(baseUri + '/admin/login', controller.login)
router.get(baseUri + '/admin/logout', controller.logout)

export default router
