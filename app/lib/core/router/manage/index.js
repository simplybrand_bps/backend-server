/**
 * 配置可用模块
 */
import user from './user'
import monitor from './monitor'
import subscription from './subscription'
import preview from './preview'
import result from './result'
import rule from './rule'

/**
 * ToDo
 * 建立目录，直观表现每个模块注册的路由
 */

export default {
    user,
    monitor,
    subscription,
    preview,
    result,
    rule
}
