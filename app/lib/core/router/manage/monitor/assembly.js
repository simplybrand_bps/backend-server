/**
 * 数据分发，仅在本文件中进行与其他模块的业务连通
 */
'use strict'
import service from '../../../components'

const addInfo = async (docs) => {
  const cbData = {
    success: true,
    message: 'OK'
  }
  await service.monitor.info.addInfo(docs)

  return cbData
}

const editinfo = async (docs) => {
  const cbData = {
    success: true,
    message: 'OK'
  }

  await service.monitor.info.editInfo(docs)

  return cbData
}

const setPrice = async (docs) => {
  const cbData = {
    success: true,
    message: 'OK'
  }

  await service.monitor.info.setPrice(docs)

  return cbData
}

const setStatus = async (docs) => {
  const cbData = {
    success: true,
    message: 'OK'
  }

  await service.monitor.info.delInfo(docs)

  return cbData
}

const infolist = async (params) => {
  const query = {
    search: params.search,
    projectId: params.projectId,
    skip: (params.page - 1) * params.size,
    size: params.size
  }
  const pageList = await service.monitor.info.infoList(query)
  if (params.size) {
    const data = [
      await service.monitor.info.infoCount(query),
      pageList
    ]

    return data
  } else return pageList
}

const infoView = async (params) => {
  const query = {
    skuid: params.skuid
  }
  const pageList = await service.monitor.info.infoView(query)
  return pageList.length? pageList[0] : {}
}

const prolist = async (params) => {
  const query = {
    search: params.search,
    skip: (params.page - 1) * params.size,
    size: params.size
  }
  const pageList = await service.extplugin.project.infoList(query)
  const pageCount = await service.extplugin.project.infoCount(query)
  if (params.size) {
    const data = [
      pageCount,
      pageList
    ]
    return data
  } else return pageList
}

export default {
  addInfo,
  editinfo,
  setStatus,
  prolist,
  infolist,
  setPrice,
  infoView
}
