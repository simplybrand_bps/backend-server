/**
 * controllers 中负责与 http 的连接工作
 * 只做简单参数的验证，传参和调起 services
 * 禁止把 rep, res 对像做为参数传递到 services 层
 * 返回数据的格式化放在这里进行
 */
import serverErr from './errors'
// import { formatted } from '../../../utils'
import service from './assembly'

const infolist = async (req, res) => {
  try {
    const bodyParams = req.query

    const params = {
      search:  bodyParams.search,
      projectId: Number.parseInt(bodyParams.subid) || Number.parseInt(bodyParams.project) || 0,
      page: Number.parseInt(bodyParams.page) || 1,
      size:  Number.parseInt(bodyParams.size) || 0
    }

    const docs = await service.infolist(params)
    return res.json({
      success: true,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const infoView = async (req, res) => {
  try {
    const bodyParams = req.query

    const params = {
      skuid:  Number.parseInt(bodyParams.skuid) || 0
    }

    const docs = await service.infoView(params)
    return res.json({
      success: true,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const addInfo = async (req, res) => {
  try {
    /**
     * post方法，读取req.body中的参数
     * @param {string} productName
     * @param {string} brand
     * @param {string} descripition
     * @param {string} series
     * @param {string} model
     * @param {string} specification
     * @param {string} industry
     * @param {string} shortName
     * @param {string} category
     * @param {string} projectId
     */
    const bodyParams = req.body

    const params = {
      productName: bodyParams.productName || '',
      brand: bodyParams.brand || '',
      description: bodyParams.description || '',
      series: bodyParams.series || '',
      model: bodyParams.model || '',
      specification: bodyParams.specification || '',
      shortName: bodyParams.shortName || '',
      industry: bodyParams.industry || '',
      category: bodyParams.category || '',
      skucode: bodyParams.skucode || '',
      price: bodyParams.price,
      projectId: Number.parseInt(bodyParams.subid) ||bodyParams.projectId || 0,
    }
    const serviceResult = await service.addInfo(params)
    return res.json(serviceResult)
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const editinfo = async (req, res) => {
  try {
    const bodyParams = req.body

    const params = {
      productName: bodyParams.productName || '',
      brand: bodyParams.brand || '',
      descripition: bodyParams.description || bodyParams.descripition || '',
      series: bodyParams.series || '',
      model: bodyParams.model || '',
      specification: bodyParams.specification || '',
      shortName: bodyParams.shortName || '',
      industry: bodyParams.industry || '',
      category: bodyParams.category || '',
      skucode: bodyParams.skucode || '',
      price: bodyParams.price,
      id: bodyParams.id
    }
    await service.editinfo(params)
    return res.json({
      success: true,
      message: 'OK'
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const setStatus = async (req, res) => {
  try {
    const bodyParams = req.body

    const params = {
      id: bodyParams.id
    }
    await service.setStatus(params)
    return res.json({
      success: true,
      message: 'OK'
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const prolist = async (req, res) => {
  try {
    const bodyParams = req.query
    const params = {
      search:  bodyParams.search,
      page: Number.parseInt(bodyParams.page) || 1,
      size:  Number.parseInt(bodyParams.size) || 0
    }

    const docs = await service.prolist(params)
    return res.json({
      success: true,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const setPrice = async (req, res) => {
  try {
    const bodyParams = req.body

    const params = {
      price: bodyParams.price,
      id: bodyParams.id
    }
    await service.setPrice(params)
    return res.json({
      success: true,
      message: 'OK'
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

export default {
  addInfo,
  editinfo,
  infoView,
  setStatus,
  prolist,
  infolist,
  setPrice
}
