/**
 * 模块访问的入口
 * 该模块不进行任何逻辑处理
 */
'use strict'
import express from 'express'
import controller from './controller'
import auth from '../../auth/controller'
const baseUri = '/sys'

const router = express.Router()

router.get(baseUri + '/monitor/list', auth.baseInfo, controller.infolist)
router.get(baseUri + '/monitor/view', auth.baseInfo, controller.infoView)
router.get(baseUri + '/monitor/project', auth.baseInfo, controller.prolist)
router.post(baseUri + '/monitor/add', auth.baseInfo, controller.addInfo)
router.put(baseUri + '/monitor/edit', auth.baseInfo, controller.editinfo)
router.put(baseUri + '/monitor/ban', auth.baseInfo, controller.setStatus)
router.put(baseUri + '/monitor/price', auth.baseInfo, controller.setPrice)


export default router
