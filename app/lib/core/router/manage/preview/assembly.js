/**
 * 数据分发，仅在本文件中进行与其他模块的业务连通
 */
'use strict'
import service from '../../../components'

const checkOrder = (orderType) => {
  const orderList = [
    "AssetID", "ShopId" 
  ]
  const allTypes = [" ASC"," DESC"]
  let orderContent = orderList[0] + allTypes[0]
  if(orderType || Number.parseInt(orderType) === 0) {
    const orderValue = (orderType/10) > orderList.length ? 0 : Math.floor(orderType/10)
    const typeValue = (orderType%10) > (allTypes.length - 1) ?  0 : orderType%10
    orderContent = orderList[orderValue] + allTypes[typeValue]
  }
  return orderContent
}

const editinfo = async (docs) => {
  const cbData = {
    success: true,
    message: 'OK'
  }

  await service.subscription.info.setInfo(docs)

  return cbData
}

const infolist = async (params) => {
  const query = {
    filter: params.filter,
    keep: params.keep,
    type: params.type,
    subid: params.subid,
    skip: (params.page - 1) * params.size,
    size: params.size
  }
  const pageList = await service.monitor.project.infoList(query)

  const data = [
    await service.monitor.project.infoCount(query),
    pageList
  ]

  return data
}

const viewinfo = async (docs) => {
  const pageList = await service.subscription.info.viewInfo(docs)

  pageList.forEach(doc => {
    doc.MatchRate = Math.ceil((doc.MatchCount * 100/ (doc.UnMatch + doc.MatchCount)) || 0)
  })

  return pageList
}

const delinfo = async (docs) => {
  const idArray =  docs.idlist.split(",")
  if (idArray.length) {
    let docparams = ''
    idArray.forEach(docid => {
      if(Number.parseInt(docid)) docparams += `(${docid}, ${docs.subid}, -1,NOW()),`
    })
    docs.content = docparams.substring(0, docparams.length -1)
    await service.monitor.project.infoDel(docs)
  }

  const cbData = {
    success: true,
    message: 'OK'
  }

  return cbData
}

const linkList = async (params) => {
  const queryid = Number.parseInt(params.skuid) || 0

  const query = {
    order: checkOrder(params.order),
    subid: params.subid,
    skuid: queryid,
    skip: (params.page - 1) * params.size,
    size: params.size
  }
  if (Number.parseInt(params.skuid) === 0) query.skuid = "0"
  if (Number.parseInt(params.checkDel)) query.isDel = "1"
  else if (Number.parseInt(params.checkDel) === 0) query.isDel = "0"

  if (params.search) {
    query.search = ''
    const keywords = params.search.split(',')
    keywords.forEach((item, index) => {
      if (index) query.search += ' AND '
      query.search += `CONCAT(AssetId,AssetTitle,IFNULL(SkuMap,""),ShopId,ShopName,AssetURL) LIKE '%${item}%'`
    })
  }
  const pageList = await service.monitor.project.linkList(query)

  const data = [
    await service.monitor.project.linkCount(query),
    pageList
  ]

  return data
}

const skuList = async (params) => {
  const query = {
    subid: params.subid,
    skuid: params.skuid
  }
  const data = await service.monitor.project.skuCheck(query)

  return data
}

export default {
  editinfo,
  infolist,
  viewinfo,
  delinfo,
  linkList,
  skuList
}
