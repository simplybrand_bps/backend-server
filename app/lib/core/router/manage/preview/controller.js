/**
 * controllers 中负责与 http 的连接工作
 * 只做简单参数的验证，传参和调起 services
 * 禁止把 rep, res 对像做为参数传递到 services 层
 * 返回数据的格式化放在这里进行
 */
import serverErr from './errors'
// import { formatted } from '../../../utils'
import service from './assembly'

const infolist = async (req, res) => {
  try {
    const bodyParams = req.query
    const params = {
      filter:  bodyParams.filter || '',
      keep:  bodyParams.keep || '',
      type: Number.parseInt(bodyParams.type) || 0,
      subid: Number.parseInt(bodyParams.subid) || 0,
      page: Number.parseInt(bodyParams.page) || 1,
      size:  Number.parseInt(bodyParams.size) || 10
    }

    const docs = await service.infolist(params)
    return res.json({
      success: true,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const editinfo = async (req, res) => {
  try {
    const bodyParams = req.body

    const params = {
      filter:  bodyParams.filter,
      keep:  bodyParams.keep,
      subid: Number.parseInt(bodyParams.subid) || 0
    }
    await service.editinfo(params)
    return res.json({
      success: true,
      message: 'OK'
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const viewinfo = async (req, res) => {
  try {
    const bodyParams = req.query

    const params = {
      subid: Number.parseInt(bodyParams.subid) || 0
    }
    const docs = await service.viewinfo(params)
    return res.json({
      success: true,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const delinfo = async (req, res) => {
  try {
    const bodyParams = req.body

    const params = {
      idlist: bodyParams.assetidlist.toString(),
      subid: Number.parseInt(bodyParams.subid) || 0
    }
    await service.delinfo(params)
    return res.json({
      success: true,
      message: 'OK'
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const getlist = async (req, res) => {
  try {
    const bodyParams = req.query
    const params = {
      search:  bodyParams.search,
      order: bodyParams.order,
      subid: Number.parseInt(bodyParams.subid) || 0,
      skuid: bodyParams.skuid,
      checkDel: bodyParams.isdel,
      page: Number.parseInt(bodyParams.page) || 1,
      size:  Number.parseInt(bodyParams.size) || 10
    }

    const docs = await service.linkList(params)
    return res.json({
      success: true,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const skuList = async (req, res) => {
  try {
    const bodyParams = req.query
    const params = {
      subid: Number.parseInt(bodyParams.subid) || 0,
      skuid: Number.parseInt(bodyParams.skuid) || 0,
    }

    const docs = await service.skuList(params)
    return res.json({
      success: true,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

export default {
  editinfo,
  infolist,
  viewinfo,
  delinfo,
  getlist,
  skuList
}
