/**
 * 模块访问的入口
 * 该模块不进行任何逻辑处理
 */
'use strict'
import express from 'express'
import controller from './controller'
import auth from '../../auth/controller'
const baseUri = '/sys'

const router = express.Router()

router.get(baseUri + '/match/list', auth.baseInfo, controller.infolist)
router.get(baseUri + '/match/view', auth.baseInfo, controller.viewinfo)
router.put(baseUri + '/match/edit', auth.baseInfo, controller.editinfo)
router.put(baseUri + '/match/del', auth.baseInfo, controller.delinfo)
router.get(baseUri + '/match/link', auth.baseInfo, controller.getlist)
router.get(baseUri + '/match/sku', auth.baseInfo, controller.skuList)

export default router
