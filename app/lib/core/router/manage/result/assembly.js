/**
 * 数据分发，仅在本文件中进行与其他模块的业务连通
 */
'use strict'
import service from '../../../components'

const editinfo = async (docs) => {
  const cbData = {
    success: true,
    message: 'OK'
  }

  await service.monitor.result.editInfo(docs)

  return cbData
}

const setinfo = async (docs) => {
  const idArray =  docs.idlist.split(",")
  if (idArray.length) {
    let docparams = ''
    idArray.forEach(docid => {
      if(Number.parseInt(docid)) docparams += `(${docid}, ${docs.subid}, NOW()),`
    })
    docs.content = docparams.substring(0, docparams.length -1)
    await service.monitor.result.setInfo(docs)
  }

  const cbData = {
    success: true,
    message: 'OK'
  }

  return cbData
}

const infolist = async (params) => {
  const query = {
    search: params.search,
    subid: params.subid,
    skip: (params.page - 1) * params.size,
    size: params.size
  }
  const pageList = await service.monitor.result.infoList(query)

  const data = [
    await service.monitor.result.infoCount(query),
    pageList
  ]

  return data
}

const todolist = async (params) => {
  const query = {
    search: params.search,
    subid: params.subid,
    check: 1
  }

  const data = await service.monitor.result.infoCount(query)

  return data
}

const delinfo = async (docs) => {
  await service.monitor.project.infoSet(docs)

  const cbData = {
    success: true,
    message: 'OK'
  }

  return cbData
}

export default {
  editinfo,
  infolist,
  setinfo,
  todolist,
  delinfo
}
