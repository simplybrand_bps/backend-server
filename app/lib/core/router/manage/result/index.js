/**
 * 模块访问的入口
 * 该模块不进行任何逻辑处理
 */
'use strict'
import express from 'express'
import controller from './controller'
import auth from '../../auth/controller'
const baseUri = '/sys'

const router = express.Router()

router.get(baseUri + '/result/list', auth.baseInfo, controller.infolist)
router.put(baseUri + '/result/edit', auth.baseInfo, controller.editinfo)
router.put(baseUri + '/result/read', auth.baseInfo, controller.setinfo)
router.get(baseUri + '/result/todo', auth.baseInfo, controller.todolist)
router.put(baseUri + '/result/set', auth.baseInfo, controller.delinfo)

export default router
