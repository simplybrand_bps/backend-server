/**
 * 数据分发，仅在本文件中进行与其他模块的业务连通
 */
'use strict'
import service from '../../../components'
import { number } from 'joi'

const checkOrder = (orderType) => {
  const orderList = [
    "Price", "CheckTime" 
  ]
  const allTypes = [" ASC"," DESC"]
  let orderContent = orderList[1] + allTypes[0]
  if(orderType || Number.parseInt(orderType) === 0) {
    const orderValue = (orderType/10) > orderList.length ? 0 : Math.floor(orderType/10)
    const typeValue = (orderType%10) > (allTypes.length - 1) ?  0 : orderType%10
    orderContent = orderList[orderValue] + allTypes[typeValue]
  }
  return orderContent
}

const addinfo = async (docs) => {
  /*
  let sqldata = ''
  docs.content.forEach(docdata => {
    sqldata += `('${docs.subid}', '${docs.skuid}','${docdata.field}','${docdata.countif}','${docdata.keywords}'),`
  })
  docs.data = sqldata.substr(0, sqldata.length - 1)
  */
  docs.data = typeof(docs.content) === "string" ? docs.content : JSON.stringify(docs.content)
  const cbData = {
    success: true,
    message: 'OK'
  }
  const sqlcontent = relizereq(docs.content)
  docs.sql = `SELECT * FROM datamatch WHERE TaskId = ${docs.subid} AND isDel = 0 AND SkuId = 0 ${sqlcontent} `
  await service.monitor.rule.addInfo(docs)

  return cbData
}

const editinfo = async (docs) => {
  docs.data = JSON.stringify(docs.content)
  const cbData = {
    success: true,
    message: 'OK'
  }
  const sqlcontent = relizereq(docs.content)
  docs.sql = `SELECT * FROM datamatch WHERE TaskId = ${docs.subid} AND isDel = 0 AND SkuId = 0 ${sqlcontent} `
  if (docs.ruleid) await service.monitor.rule.editInfo(docs)
  else await service.monitor.rule.addInfo(docs)

  return cbData
}

const delinfo = async (docs) => {
  const cbData = {
    success: true,
    message: 'OK'
  }

  await service.monitor.rule.delInfo(docs)

  return cbData
}

const viewinfo = async (docs) => {
  
  const pageList = await service.monitor.rule.viewInfo(docs)

  return pageList
}

const infolist = async (params) => {
  const query = {
    subid: params.subid,
    skuid: params.skuid,
    type: params.type,
    search: params.search,
    order: checkOrder(params.order),
    skip: (params.page - 1) * params.size,
    size: params.size
  }
  const pageList = await service.monitor.result.matchList(query)

  const data = [
    await service.monitor.result.matchCount(query),
    pageList
  ]

  return data
}

const setinfo = async (docs) => {

  const idArray =  docs.idlist.split(",")
  if (idArray.length) {
    let docparams = ''
    idArray.forEach(docid => {
      if(Number.parseInt(docid)) docparams += `(${docid}, ${docs.subid}, ${docs.skuid}, NOW()),`
    })
    docs.content = docparams.substring(0, docparams.length -1)
    await service.monitor.result.checkMatch(docs)
  }

  const cbData = {
    success: true,
    message: 'OK'
  }

  return cbData
}

const setitem = async (docs) => {
  const cbData = {
    success: true,
    message: 'OK'
  }

  await service.monitor.result.setMatch(docs)

  return cbData
}

const todolist = async (params) => {
  const query = {
    subid: params.subid,
    skuid: params.skuid,
    type: params.type,
    search: params.search,
    check: 1
  }

  const data = await service.monitor.result.matchCount(query)

  return data
}

const relizereq = (jsonstr) => {
  let rulesql = ' '
  if (!jsonstr) return rulesql
  const doc = typeof(jsonstr) === "string" ? JSON.parse(jsonstr) : JSON.parse(JSON.stringify(jsonstr))
  if (doc.rulegroup && doc.rulegroup.length) {
    const rulestatus = ['AND','OR']
    const groupstatus = ['LIKE','LIKE','NOT LIKE','BETWEEN']
  
    rulesql += 'AND '
    if (doc.searchstatus) rulesql += 'NOT '
      rulesql += ' ('

  for(let gindex in doc.rulegroup){
    const ruledoc = doc.rulegroup[gindex]
    let laststr = ''
    if (gindex > 0) laststr += ` ${rulestatus[doc.rulestatus]} `
    laststr += '('
    let basestr = ''
    let extsql = ''
    if (ruledoc.field.indexOf('+') > 0) extsql += '( ChannelId IN (1,2,30) AND ('

    if (ruledoc.status < 3) {
      for(let exindex in ruledoc.keywords) {
        if (exindex > 0) {
          extsql += ruledoc.status == 1 ? ` OR `: ` AND `
          basestr += ruledoc.status == 1 ? ` OR `: ` AND `
        }
        // 使用CONCAT时，要替换Null
        if (ruledoc.field.indexOf('+') > 0) {
          const extArray = ruledoc.field.split('+')
          extsql += `(CONCAT(${extArray[0]}, IFNULL(${extArray[1]},"")) ${groupstatus[ruledoc.status]} "%${ruledoc.keywords[exindex].replace('%', '\\%')}%") `
          basestr += `(${extArray[0]} ${groupstatus[ruledoc.status]} "%${ruledoc.keywords[exindex].replace('%', '\\%')}%" `
          // 使用NOT LIKE时，要额外判定Null
          if (ruledoc.status == 2) basestr += `OR ${extArray[0]} IS NULL`
          basestr += ') '
        } else {
          basestr += `(${ruledoc.field} ${groupstatus[ruledoc.status]} "%${ruledoc.keywords[exindex].replace('%', '\\%')}%" `
          // 使用NOT LIKE时，要额外判定Null
          if (ruledoc.status == 2) basestr += `OR ${ruledoc.field} IS NULL`
          basestr += ') '

        }
      }
    } else if (ruledoc.status == 3) {
      basestr += `(${ruledoc.field} ${groupstatus[ruledoc.status]} ${ruledoc.keywords[0]} AND ${ruledoc.keywords[1]})`
    }
    if (ruledoc.field.indexOf('+') > 0) {
      extsql += `)) OR (ChannelId NOT IN (1,2,30) AND (${basestr}))`
      laststr += `${extsql})`
    } else laststr += `${basestr})`
      rulesql += `${laststr}`
    }
    rulesql += ') '
  }

  return rulesql
}

const matchlist = async (params) => {
  
  const query = {
    subid: params.subid,
    skuid: params.skuid,
    type: params.type,
    rule: relizereq(params.rule),
    skip: (params.page - 1) * params.size,
    size: params.size
  }
  const pageList = await service.monitor.result.prematchList(query)

  const data = [
    await service.monitor.result.prematchCount(query),
    pageList
  ]

  return data
}

const viewstatus = async (params) => {
  
  const query = {
    subid: params.subid,
    skuid: params.skuid
  }
  const ruleList = await service.monitor.rule.viewstatus(query)

  return ruleList
}

const setstatus = async (docs) => {
  const ruleList = await service.monitor.rule.ruleinfo(docs)
  if (ruleList.length) {
    const sqlcontent = relizereq(JSON.parse(ruleList[0].rulecontent.replace(/^\"|\"$/g,'')))
    if(sqlcontent.length) {
      const idArray = await service.monitor.rule.getDataId({subid:ruleList[0].taskid,sqlcontent})
      let docparams = ''
      idArray.forEach(docid => {
        if(Number.parseInt(docid.AssetID)) docparams += `(${docid.AssetID}, ${ruleList[0].taskid}, ${docs.skuid}),`
      })
      docs.content = docparams.substring(0, docparams.length -1)
      if(docparams.length) await service.monitor.rule.checkMatch(docs)
    }
    service.monitor.rule.setFinish(docs)
  }

  return ruleList

}

export default {
  addinfo,
  editinfo,
  delinfo,
  viewinfo,
  infolist,
  setinfo,
  setitem,
  todolist,
  matchlist,
  viewstatus,
  setstatus
}
