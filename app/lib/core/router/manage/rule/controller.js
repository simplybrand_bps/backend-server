/**
 * controllers 中负责与 http 的连接工作
 * 只做简单参数的验证，传参和调起 services
 * 禁止把 rep, res 对像做为参数传递到 services 层
 * 返回数据的格式化放在这里进行
 */
import serverErr from './errors'
// import { formatted } from '../../../utils'
import service from './assembly'

const viewinfo = async (req, res) => {
  try {
    const bodyParams = req.query

    const params = {
      subid: Number.parseInt(bodyParams.subid) || Number.parseInt(bodyParams.taskid) ||0,
      skuid: Number.parseInt(bodyParams.skuid) || 0
    }

    const docs = await service.viewinfo(params)
    return res.json({
      success: true,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const addinfo = async (req, res) => {
  try {
    const bodyParams = req.body

    const params = {
      content:  bodyParams.content,
      skuid:  bodyParams.skuid,
      subid: Number.parseInt(bodyParams.subid) || Number.parseInt(bodyParams.taskid) ||0
    }
    await service.addinfo(params)
    return res.json({
      success: true,
      message: 'OK'
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const editinfo = async (req, res) => {
  try {
    const bodyParams = req.body

    const params = {
      content:  bodyParams.content,
      skuid:  bodyParams.skuid,
      subid: Number.parseInt(bodyParams.subid) || Number.parseInt(bodyParams.taskid) ||0,
      ruleid: Number.parseInt(bodyParams.ruleid) || 0
    }
    await service.editinfo(params)
    return res.json({
      success: true,
      message: 'OK'
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const delinfo = async (req, res) => {
  try {
    const bodyParams = req.body

    const params = {
      ruleid: Number.parseInt(bodyParams.ruleid) || 0
    }
    await service.delinfo(params)
    return res.json({
      success: true,
      message: 'OK'
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const viewlist = async (req, res) => {
  try {
    const bodyParams = req.query

    const params = {
      subid: Number.parseInt(bodyParams.subid) || Number.parseInt(bodyParams.taskid) ||0,
      skuid: Number.parseInt(bodyParams.skuid) || 0,
      search: bodyParams.search || '',
      order: bodyParams.order,
      page: Number.parseInt(bodyParams.page) || 1,
      size: Number.parseInt(bodyParams.size) || 10,
      type: Number.parseInt(bodyParams.type) || 0
    }

    const docs = await service.infolist(params)
    return res.json({
      success: true,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const setitem = async (req, res) => {
  try {
    const bodyParams = req.body

    const params = {
      assetid:  bodyParams.assetid,
      skuid:  bodyParams.skuid,
      subid: Number.parseInt(bodyParams.subid) || Number.parseInt(bodyParams.taskid) ||0
    }
    await service.setitem(params)
    return res.json({
      success: true,
      message: 'OK'
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const setinfo = async (req, res) => {
  try {
    const bodyParams = req.body

    const params = {
      idlist: bodyParams.assetidlist.toString(),
      skuid:  bodyParams.skuid,
      subid: Number.parseInt(bodyParams.subid) || Number.parseInt(bodyParams.taskid) ||0
    }
    await service.setinfo(params)
    return res.json({
      success: true,
      message: 'OK'
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const todolist = async (req, res) => {
  try {
    const bodyParams = req.query

    const params = {
      subid: Number.parseInt(bodyParams.subid) || Number.parseInt(bodyParams.taskid) ||0,
      skuid: Number.parseInt(bodyParams.skuid) || 0,
      search: bodyParams.search || '',
      type: Number.parseInt(bodyParams.type) || 0
    }

    const docs = await service.todolist(params)
    return res.json({
      success: true,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const matchlist = async (req, res) => {
  try {
    const bodyParams = req.body
    const params = {
      subid: Number.parseInt(bodyParams.subid) || Number.parseInt(bodyParams.taskid) ||0,
      rule: bodyParams.rule,
      page: Number.parseInt(bodyParams.page) || 1,
      size: Number.parseInt(bodyParams.size) || 10
    }
    const docs = await service.matchlist(params)
    return res.json({
      success: true,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const viewstatus = async (req, res) => {
  try {
    const bodyParams = req.query

    const params = {
      skuid: Number.parseInt(bodyParams.skuid) || 0,
      ruleid: Number.parseInt(bodyParams.ruleid) || 0
    }
    const docs = await service.viewstatus(params)
    return res.json({
      success: true,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const setstatus = async (req, res) => {
  try {
    const bodyParams = req.body

    const params = {
      skuid: Number.parseInt(bodyParams.skuid) || 0,
      ruleid: Number.parseInt(bodyParams.ruleid) || 0
    }
    await service.setstatus(params)
    return res.json({
      success: true,
      message: 'OK'
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

export default {
  editinfo,
  addinfo,
  delinfo,
  viewinfo,
  viewlist,
  setitem,
  setinfo,
  todolist,
  matchlist,
  viewstatus,
  setstatus
}
