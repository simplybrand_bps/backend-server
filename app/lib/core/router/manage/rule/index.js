/**
 * 模块访问的入口
 * 该模块不进行任何逻辑处理
 */
'use strict'
import express from 'express'
import controller from './controller'
import auth from '../../auth/controller'
const baseUri = '/sys'

const router = express.Router()

router.get(baseUri + '/rule/view', auth.baseInfo, controller.viewinfo)
router.post(baseUri + '/rule/add', auth.baseInfo, controller.addinfo)
router.put(baseUri + '/rule/edit', auth.baseInfo, controller.editinfo)
router.put(baseUri + '/rule/del', auth.baseInfo, controller.delinfo)
router.get(baseUri + '/rule/list', auth.baseInfo, controller.viewlist)
router.put(baseUri + '/rule/item', auth.baseInfo, controller.setitem)
router.put(baseUri + '/rule/read', auth.baseInfo, controller.setinfo)
router.get(baseUri + '/rule/todo', auth.baseInfo, controller.todolist)
router.put(baseUri + '/rule/match', auth.baseInfo, controller.matchlist)
router.get(baseUri + '/rule/status', auth.baseInfo, controller.viewstatus)
router.put(baseUri + '/rule/active', auth.baseInfo, controller.setstatus)

export default router
