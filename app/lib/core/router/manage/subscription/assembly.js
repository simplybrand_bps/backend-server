/**
 * 数据分发，仅在本文件中进行与其他模块的业务连通
 */
'use strict'
import service from '../../../components'

const addInfo = async (docs) => {
  const cbData = {
    success: true,
    message: 'OK'
  }
  await service.subscription.info.addInfo(docs)

  return cbData
}

const editinfo = async (docs) => {
  const cbData = {
    success: true,
    message: 'OK'
  }

  await service.subscription.info.editInfo(docs)
  await service.subscription.policy.editInfo(docs)

  return cbData
}

const setStatus = async (docs) => {
  const cbData = {
    success: true,
    message: 'OK'
  }

  await service.subscription.policy.delInfo(docs)

  return cbData
}

const infolist = async (params) => {
  const query = {
    search: params.search,
    skip: (params.page - 1) * params.size,
    size: params.size
  }
  const pageList = await service.subscription.policy.infoList(query)

  const data = [
    await service.subscription.policy.infoCount(query),
    pageList
  ]

  return data
}

const setInfo = async (docs) => {
  const cbData = {
    success: true,
    message: 'OK'
  }

  await service.subscription.policy.addInfo(docs)

  return cbData
}

export default {
  addInfo,
  editinfo,
  setInfo,
  setStatus,
  infolist
}
