/**
 * controllers 中负责与 http 的连接工作
 * 只做简单参数的验证，传参和调起 services
 * 禁止把 rep, res 对像做为参数传递到 services 层
 * 返回数据的格式化放在这里进行
 */
import serverErr from './errors'
// import { formatted } from '../../../utils'
import service from './assembly'

const infolist = async (req, res) => {
  try {
    const bodyParams = req.query
    const params = {
      search:  bodyParams.search,
      page: Number.parseInt(bodyParams.page) || 1,
      size:  Number.parseInt(bodyParams.size) || 10
    }

    const docs = await service.infolist(params)
    return res.json({
      success: true,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const addInfo = async (req, res) => {
  try {
    /**
     * post方法，读取req.body中的参数
     * @param {string} name
     * @param {string} mainId
     * @param {string} init
     * @param {string} expire
     * @param {string} brandid
     * @param {string} packageid
     * @param {string} channel
     */
    const bodyParams = req.body

    const params = {
      brandid: bodyParams.brandid || bodyParams.brand,
      mainid: bodyParams.mainid,
      name: bodyParams.name,
      init: bodyParams.init || '',
      expire: bodyParams.expire || '',
      packageid: bodyParams.packageid || '',
      channel: bodyParams.channel || '',
      userid: bodyParams.userid,
      channelname: bodyParams.channelname || '',
      industry: bodyParams.industry || '其他行业',
      category: bodyParams.category || '全品类',
      series: bodyParams.series || '其他系列',
      model: bodyParams.model || '其他型号'
    }
    const serviceResult = await service.addInfo(params)
    return res.json(serviceResult)
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const editInfo = async (req, res) => {
  try {
    const bodyParams = req.body

    const params = {
      poid: bodyParams.poid,
      subid: bodyParams.subid,
      name: bodyParams.name,
      init: bodyParams.init || '',
      expire: bodyParams.expire || '',
      packageid: bodyParams.packageid || '',
      channel: bodyParams.channel || '',
      userid: bodyParams.userid,
      mainid: bodyParams.mainid,
      channelname: bodyParams.channelname || '',
      industry: bodyParams.industry || '其他行业',
      category: bodyParams.category || '全品类',
      series: bodyParams.series || '其他系列',
      model: bodyParams.model || '其他型号'
    }
    await service.editinfo(params)
    return res.json({
      success: true,
      message: 'OK'
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const setStatus = async (req, res) => {
  try {
    const bodyParams = req.body

    const params = {
      poid: Number.parseInt(bodyParams.poid) || 0,
      status: Number.parseInt(bodyParams.status) || 0
    }
    await service.setStatus(params)
    return res.json({
      success: true,
      message: 'OK'
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const setInfo = async (req, res) => {
  try {
    const bodyParams = req.body

    const params = {
      init: bodyParams.init || '',
      expire: bodyParams.expire || '',
      poid: bodyParams.poid
    }
    await service.setInfo(params)
    return res.json({
      success: true,
      message: 'OK'
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

export default {
  addInfo,
  editInfo,
  setInfo,
  setStatus,
  infolist
}
