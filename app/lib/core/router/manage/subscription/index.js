/**
 * 模块访问的入口
 * 该模块不进行任何逻辑处理
 */
'use strict'
import express from 'express'
import controller from './controller'
import auth from '../../auth/controller'
const baseUri = '/sys'

const router = express.Router()

router.get(baseUri + '/subscription/list', auth.baseInfo, controller.infolist)
router.post(baseUri + '/subscription/reset', auth.baseInfo, controller.setInfo)
router.post(baseUri + '/subscription/add', auth.baseInfo, controller.addInfo)
router.put(baseUri + '/subscription/edit', auth.baseInfo, controller.editInfo)
router.put(baseUri + '/subscription/ban', auth.baseInfo, controller.setStatus)


export default router
