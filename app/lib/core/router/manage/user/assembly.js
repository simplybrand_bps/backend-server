/**
 * 数据分发，仅在本文件中进行与其他模块的业务连通
 */
'use strict'
import service from '../../../components'

/**
 * 生成激活信息，submitted为用户提交的数据，其内容为：
 * @param {string} company
 * @param {string} email
 * @param {string} password
 * @param {string} phone
 * @param {string} username
 */

const checkEmail = async (docs) => {
  const userInfo = await service.user.info.checkAccount(docs)
  return userInfo
}

const singup = async (docs) => {
  const cbData = {
    success: true,
    message: 'OK'
  }
  const user = await service.user.info.checkAccount(docs)
  if (user.length) {
    cbData.success = false,
    cbData.message = 'existed'
    return cbData
  }

    // throw new Error(`Error happens with the status code: 406, the key is: ${docs.email}`)
  const tokenInfo = await service.identify.password.genPwd(docs.password)
  // 生成待激活记录
  const result = await service.user.info.addAccount(docs, tokenInfo)

  return cbData
}

const editinfo = async (docs) => {
  const cbData = {
    success: true,
    message: 'OK'
  }

  await service.user.info.editInfo(docs)

  return cbData
}

const changeAccount = async (docs) => {
  const cbData = {
    success: true,
    message: 'OK'
  }
  const user = await service.user.info.checkAccount(docs)
  if (user.length) {
    cbData.success = false,
    cbData.message = 'existed'
    return cbData
  } else await service.user.info.editAcoout(docs)

  return cbData
}

const changePassword = async (docs) => {
  const cbData = {
    success: true,
    message: 'OK'
  }

  // throw new Error(`Error happens with the status code: 406, the key is: ${docs.email}`)
  const tokenInfo = await service.identify.password.genPwd(docs.password)
    // 生成待激活记录
  await service.user.info.editPassword(docs, tokenInfo)

  return cbData
}

const setAccount = async (docs) => {
  const cbData = {
    success: true,
    message: 'OK'
  }

  const statusDocs = ['pending', 'active']
  docs.setting = docs.status  ? statusDocs[1] : statusDocs[0]
  await service.user.info.setAccount(docs)

  return cbData
}

const userlist = async (params) => {
  const query = {
    search: params.search,
    skip: (params.page - 1) * params.size,
    size: params.size
  }
  const pageList = await service.user.info.infoList(query)
  if (params.size) {
    const data = [
      await service.user.info.infoCount(query),
      pageList
    ]

    return data
  } else return pageList
}

export default {
  singup,
  checkEmail,
  editinfo,
  changeAccount,
  changePassword,
  setAccount,
  userlist
}
