/**
 * controllers 中负责与 http 的连接工作
 * 只做简单参数的验证，传参和调起 services
 * 禁止把 rep, res 对像做为参数传递到 services 层
 * 返回数据的格式化放在这里进行
 */
import serverErr from './errors'
// import { formatted } from '../../../utils'
import service from './assembly'

const Validater = {
  // 邮箱必须包含@符号，@前长度50，@字符后长度50
  email: (value) => {
    if (typeof (value) !== 'string') return false
    const arr = value.split('@')
    if (arr.length !== 2) return false
    if (arr[0].length > 50 || arr[1].length > 50) return false
    return true
  },
  // 账户名格式为中英文+数字,5-20个字符，一个中文字为两个字符
  username: (value) => {
    if (typeof (value) !== 'string' || value.length > 50) return false
    return true
  },
  // 密码要求包含5-25个字符，只可以是字母、数字和符号，其中至少包含两种
  password: (value) => {
    if (typeof (value) !== 'string' || value.length > 25) return false
    return true
  },
  company: (value) => {
    if (typeof (value) !== 'string' || value.length > 250) return false
    return true
  },
  phone: (value) => {
    if (value && !(/^1[3456789]\d{9}$|^\+861[3456789]\d{9}$|^861[3456789]\d{9}/g.test(value))) return false
    return true
  }
}

const paramsFormat = (bodyParams) => {
  const paramHandler = {
    get: (target, key) => {
      if (Validater[key]) {
        if (Validater[key](target[key])) return target[key]
        else throw new Error(`Error happens with the status code: 400, the key is: ${key}`)
      }
    }
  }
  const bodyParse = new Proxy(bodyParams, paramHandler)
  return bodyParse
}

const userlist = async (req, res) => {
  try {
    const bodyParams = req.query
    const params = {
      search: bodyParams.search,
      page: Number.parseInt(bodyParams.page) || 1,
      size:  Number.parseInt(bodyParams.size) || 0
    }

    const docs = await service.userlist(params)
    return res.json({
      success: true,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

// 注册
const singup = async (req, res) => {
  try {
    /**
     * post方法，读取req.body中的参数
     * @param {string} username
     * @param {string} email
     * @param {string} password
     * @param {string} company
     * @param {string} phone
     */
    const bodyParams = req.body

    const bodyParse = paramsFormat(bodyParams)
    const params = ((
      {
        email,
        username,
        password,
        company,
        phone
      }) => (
      {
        email,
        username,
        password,
        company,
        phone,
        url: bodyParams.url
      }
    ))(bodyParse)
    const serviceResult = await service.singup(params)
    return res.json(serviceResult)
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const editinfo = async (req, res) => {
  try {
    const bodyParams = req.body

    const params = {
      id: bodyParams.userid,
      username: bodyParams.username,
      company: bodyParams.company,
      phone: bodyParams.phone || '',
      url: bodyParams.url || ''
    }
    await service.editinfo(params)
    return res.json({
      success: true,
      message: 'OK'
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const editEmail = async (req, res) => {
  try {
    const bodyParams = req.body

    const params = {
      id: bodyParams.userid,
      email: bodyParams.email
    }
    await service.changeAccount(params)
    return res.json({
      success: true,
      message: 'OK'
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const editPwd = async (req, res) => {
  try {
    const bodyParams = req.body

    const params = {
      id: bodyParams.userid,
      password: bodyParams.password
    }
    await service.changePassword(params)
    return res.json({
      success: true,
      message: 'OK'
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const editStatus = async (req, res) => {
  try {
    const bodyParams = req.body

    const params = {
      id: bodyParams.userid,
      status: Number.parseInt(bodyParams.status) || 0
    }
    await service.setAccount(params)
    return res.json({
      success: true,
      message: 'OK'
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const checkUser = async (req, res) => {
  try {
    const bodyParams = req.query

    const params = {
      email: bodyParams.email,
      type: Number.parseInt(bodyParams.type) || 0
    }
    const cbData = await service.checkEmail(params)
    return res.json({
      success: true,
      message: 'OK',
      results: cbData
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

export default {
  singup,
  editinfo,
  editEmail,
  editPwd,
  editStatus,
  userlist,
  checkUser
}
