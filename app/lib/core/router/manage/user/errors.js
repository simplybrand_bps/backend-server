/**
 * 统一的错误处理
 */
const logger = global.logger

const errorCode = {
  400: 'The param is illegal: ',
  403: 'Username/Account/E-mail already taken: ',
  406: '`Encountered error when creating user: ',
  500: 'Internal server error, please try again later!'
}
const errorDecoupling = (str) => {
  const cberr = {
    code: 500,
    message: str
  }
  try {
    const [, codeStr, , key] = str.split(/[:,]/)
    const code = Number.parseInt(codeStr)
    if (code && code in errorCode) {
      cberr.code = code
      cberr.message = errorCode[code] + key
    }
    return cberr
  } catch (e) {
    cberr.message = errorCode[500]
    logger.warn(e.stack)
    return cberr
  }
}

const serverErr = (e) => {
  const bcJson = {
    success: false,
    status: 500,
    message: errorCode[500]
  }
  if (e.message && e.message.startsWith('Error happens with the status code:')) {
    logger.info(e.message)
    const eStack = errorDecoupling(e.message)
    bcJson.status = eStack.code
    bcJson.message = eStack.message
    return { eStack, bcJson }
  } else {
    logger.warn(e.stack)
    const eStack = {
      code: 500,
      message: errorCode[500]
    }
    return { eStack, bcJson }
  }
}

logger.silly('model: register')
logger.silly('router:')
logger.silly('   post /users/signup')
logger.silly('   put  /verify/activate')

export default serverErr
