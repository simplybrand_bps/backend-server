/**
 * 模块访问的入口
 * 该模块不进行任何逻辑处理
 */
'use strict'
import express from 'express'
import controller from './controller'
import auth from '../../auth/controller'
const baseUri = '/sys'

const router = express.Router()

router.get(baseUri + '/users/list', auth.baseInfo, controller.userlist)
router.get(baseUri + '/users/check', auth.baseInfo, controller.checkUser)
router.post(baseUri + '/users/add', auth.baseInfo, controller.singup)
router.put(baseUri + '/users/edit', auth.baseInfo, controller.editinfo)
router.put(baseUri + '/users/change', auth.baseInfo, controller.editEmail)
router.put(baseUri + '/users/reset', auth.baseInfo, controller.editPwd)
router.put(baseUri + '/users/ban', auth.baseInfo, controller.editStatus)

export default router
