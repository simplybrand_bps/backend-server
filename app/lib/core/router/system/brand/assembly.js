/**
 * 数据分发，仅在本文件中进行与其他模块的业务连通
 */
'use strict'
import service from '../../../components'

const addBrand = async (docs) => {
  const cbData = {
    success: true,
    message: 'OK'
  }
  const result = await service.system.brand.addInfo(docs)

  return cbData
}

const editinfo = async (docs) => {
  const cbData = {
    success: true,
    message: 'OK'
  }

  await service.system.brand.editInfo(docs)

  return cbData
}

const setStatus = async (docs) => {
  const cbData = {
    success: true,
    message: 'OK'
  }

  await service.system.brand.delInfo(docs)

  return cbData
}

const brandlist = async (params) => {
  if (params.type === 2) {
    const pageList = await service.system.brand.getInfo(params)
    return pageList
  } else {
    const query = {
      search: params.search,
      type: params.type,
      skip: (params.page - 1) * params.size,
      size: params.size
    }
    const pageList = await service.system.brand.infoList(query)
    if (params.size) {
      const data = [
        await service.system.brand.infoCount(query),
        pageList
      ]
    
      return data
    } else return pageList
  }

}
export default {
  addBrand,
  editinfo,
  setStatus,
  brandlist
}
