/**
 * controllers 中负责与 http 的连接工作
 * 只做简单参数的验证，传参和调起 services
 * 禁止把 rep, res 对像做为参数传递到 services 层
 * 返回数据的格式化放在这里进行
 */
import path from 'path'
import serverErr from './errors'
// import { formatted } from '../../../utils'
import service from './assembly'

const brandlist = async (req, res) => {
  try {
    const bodyParams = req.query
    const params = {
      search:  bodyParams.search,
      page: Number.parseInt(bodyParams.page) || 1,
      size:  Number.parseInt(bodyParams.size) || 0,
      type: Number.parseInt(bodyParams.type) || 0,
    }

    const docs = await service.brandlist(params)
    return res.json({
      success: true,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const addBrand = async (req, res) => {
  try {
    /**
     * post方法，读取req.body中的参数
     * @param {string} name
     * @param {string} brand
     * @param {string} trademark
     * @param {string} keywords
     * @param {string} logo
     */
    const bodyParams = req.body

    const params = {
      name: bodyParams.name,
      brand: bodyParams.brand || '',
      trademark: bodyParams.trademark || '',
      keywords: bodyParams.keywords || bodyParams.name,
      logo: bodyParams.logo || ''
    }
    const serviceResult = await service.addBrand(params)
    return res.json(serviceResult)
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const editinfo = async (req, res) => {
  try {
    const bodyParams = req.body

    const params = {
      id: bodyParams.brandid,
      name: bodyParams.name,
      brand: bodyParams.brand || '',
      trademark: bodyParams.trademark || '',
      keywords: bodyParams.keywords || bodyParams.name,
      logo: bodyParams.logo || ''
    }
    await service.editinfo(params)
    return res.json({
      success: true,
      message: 'OK'
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const upload = async (req, res) => {
  try {

    return res.json({
      success: true,
      message: 'OK',
      result: req.file.filename
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const upSetting = {
  destination: function (_req, _file, cb) {
    cb(null, path.join(__dirname, `../../../../../../public/logos/`))
  },
  filename: function (_req, file, cb) {
    const singfileArray = file.originalname.split('.');
    const fileExtension = singfileArray[singfileArray.length - 1];
    cb(null, singfileArray[0] + '@' + Date.now() + "." + fileExtension);
  }
}

const setStatus = async (req, res) => {
  try {
    const bodyParams = req.body

    const params = {
      id: bodyParams.brandid,
      status: Number.parseInt(bodyParams.status) || 0
    }
    await service.setStatus(params)
    return res.json({
      success: true,
      message: 'OK'
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

export default {
  addBrand,
  editinfo,
  upload,
  setStatus,
  brandlist,
  upSetting
}
