/**
 * 模块访问的入口
 * 该模块不进行任何逻辑处理
 */
'use strict'
import express from 'express'
import multer from 'multer'
import controller from './controller'
import auth from '../../auth/controller'
const upload = multer({ storage: multer.diskStorage(controller.upSetting) })
const baseUri = '/sys'

const router = express.Router()

router.get(baseUri + '/brand/list', auth.baseInfo, controller.brandlist)
router.post(baseUri + '/brand/upload', auth.baseInfo, upload.single('logo'), controller.upload)
router.post(baseUri + '/brand/add', auth.baseInfo, controller.addBrand)
router.put(baseUri + '/brand/edit', auth.baseInfo, controller.editinfo)
router.put(baseUri + '/brand/ban', auth.baseInfo, controller.setStatus)

export default router