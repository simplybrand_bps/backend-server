/**
 * 数据分发，仅在本文件中进行与其他模块的业务连通
 */
'use strict'
import service from '../../../components'

const infolist = async (params) => {
  const query = {
    search: params.search,
    skip: (params.page - 1) * params.size,
    size: params.size
  }
  const pageList = await service.system.channel.infoList(query)
  if (params.size) {
    const data = [
      await service.system.channel.infoCount(query),
      pageList
    ]
    return data
  } else  return pageList
}

export default {
  infolist
}
