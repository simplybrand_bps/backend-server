/**
 * 数据分发，仅在本文件中进行与其他模块的业务连通
 */
'use strict'
import service from '../../../components'

const infofilter = async () => {
  const pageList = await service.system.image.infofilter()
  return pageList
}

const infolist = async (params) => {
  const query = {
    datatime: params.datatime,
    skip: (params.page - 1) * params.size,
    size: params.size
  }
  if(params.datacheck) {
    if(Number.parseInt(params.datacheck) === 1) query.checkNu = 1
    else query.checkNo = 1
  }
  const pageList = await service.system.image.infoList(query)
  if (params.size) {
    const data = [
      await service.system.image.infoCount(query),
      pageList
    ]
    return data
  } else  return pageList
}

const setstatus = async (docs) => {
  const cbData = {
    success: true,
    message: 'OK'
  }
  await service.system.image.setstatus(docs)
  return cbData
}

const listfilter = async () => {
  const pageList = await service.system.image.listfilter()
  return pageList
}

const listdata = async (params) => {
  const query = {
    docdate: params.docdate,
    start: params.start,
    end: params.end
  }
  const pageList = await service.system.image.listdata(query)
  return pageList
}

export default {
  infofilter,
  infolist,
  setstatus,
  listfilter,
  listdata
}
