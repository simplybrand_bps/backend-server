/**
 * controllers 中负责与 http 的连接工作
 * 只做简单参数的验证，传参和调起 services
 * 禁止把 rep, res 对像做为参数传递到 services 层
 * 返回数据的格式化放在这里进行
 */
import serverErr from './errors'
import service from './assembly'

const infofilter = async (_req, res) => {
  try {
    const docs = await service.infofilter()
    return res.json({
      success: true,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const infolist = async (req, res) => {
  try {
    const bodyParams = req.query
    const params = {
      search:  bodyParams.search,
      datatime:  bodyParams.datatime,
      datacheck:  bodyParams.datacheck,
      page: Number.parseInt(bodyParams.page) || 1,
      size:  Number.parseInt(bodyParams.size) || 0
    }
    const docs = await service.infolist(params)
    return res.json({
      success: true,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const setstatus = async (req, res) => {
  try {
    const bodyParams = req.body

    const params = {
      id: Number.parseInt(bodyParams.id) || 0,
      status: bodyParams.status || 0
    }
    await service.setstatus(params)
    return res.json({
      success: true,
      message: 'OK'
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const listfilter = async (_req, res) => {
  try {
    const docs = await service.listfilter()
    return res.json({
      success: true,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const listdata = async (req, res) => {
  try {
    const bodyParams = req.query
    const params = {
      docdate:  bodyParams.docdate,
      start:  bodyParams.start,
      end:  bodyParams.end
    }
    const docs = await service.listdata(params)
    return res.json({
      success: true,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

export default {
  infofilter,
  infolist,
  setstatus,
  listfilter,
  listdata
}
