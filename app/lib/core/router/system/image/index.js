/**
 * 模块访问的入口
 * 该模块不进行任何逻辑处理
 */
'use strict'
import express from 'express'
import controller from './controller'
import auth from '../../auth/controller'
const baseUri = '/sys'

const router = express.Router()

router.get(baseUri + '/image/filter', auth.baseInfo, controller.infofilter)
router.get(baseUri + '/image/list', auth.baseInfo, controller.infolist)
router.put(baseUri + '/image/set', auth.baseInfo, controller.setstatus)
router.get(baseUri + '/image/check', auth.baseInfo, controller.listfilter)
router.get(baseUri + '/image/paging', auth.baseInfo, controller.listdata)

export default router