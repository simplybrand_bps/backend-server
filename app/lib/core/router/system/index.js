/**
 * 配置可用模块
 */
import brand from './brand'
import channel from './channel'
import level from './level'
import image from './image'

/**
 * ToDo
 * 建立目录，直观表现每个模块注册的路由
 */

export default {
    brand,
    channel,
    level,
    image
}