/**
 * controllers 中负责与 http 的连接工作
 * 只做简单参数的验证，传参和调起 services
 * 禁止把 rep, res 对像做为参数传递到 services 层
 * 返回数据的格式化放在这里进行
 */
import serverErr from './errors'
import service from './assembly'

const infolist = async (req, res) => {
  try {
    const bodyParams = req.query
    const params = {
      search:  bodyParams.search,
      page: Number.parseInt(bodyParams.page) || 1,
      size:  Number.parseInt(bodyParams.size) || 0
    }

    const docs = await service.infolist(params)
    return res.json({
      success: true,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

export default {
  infolist
}
