import jwt from 'jsonwebtoken'
import uuidv4 from 'uuid/v4'

const genJwtToken = async (account, authConfig) => {
  const uuid = uuidv4()
  const token = jwt.sign({ id: uuid, account, expire: authConfig.expiresIn }, authConfig.secretKey)
  return {
    uuid,
    token
  }
}

const verifyJWT = async (token, authConfig) => {
  const jwtInfo = jwt.verify(token, authConfig.secretKey)
  return { uuid: jwtInfo.id, account: jwtInfo.account, expire: jwtInfo.expire }
}

export default {
  genJwtToken,
  verifyJWT
}
