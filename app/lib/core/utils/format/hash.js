import crypto from 'crypto'

const md5 = (somestr) => {
  return crypto
    .createHash('md5')
    .update(somestr)
    .digest('hex')
}

const compared = (property) => {
  return (a, b) => {
    return b[property] - a[property]
  }
}

export {
  md5,
  compared
}
