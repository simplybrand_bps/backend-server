'use strict'

import path from 'path'
import nconf from 'nconf'
import express from 'express'

const router = express.Router();

(async () => {
  const hasDocs = await nconf.get('docs')
  if (hasDocs) {
    router.use('/docs', express.static(path.join(__dirname, '../../../../docs')))
  }
})()

export default router
