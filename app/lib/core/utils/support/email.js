/* eslint no-console: ["error", { allow: ["log"] }] */
import nodemailer from 'nodemailer'
import nconf from 'nconf'
const logger = global.logger

const transporter = async () => {
  const emailConfig = await nconf.get('email')
  return nodemailer.createTransport({
    host: 'hwsmtp.exmail.qq.com',
    port: 465,
    secure: true,
    auth: {
      type: 'login',
      user: process.env.EMAIL_USER ? process.env.EMAIL_USER : emailConfig.username,
      pass: process.env.EMAIL_PASSWORD ? process.env.EMAIL_PASSWORD : emailConfig.password
    }
  })
}

const getPasswordResetHtmlLink = async (email, uuid, passwordToken) => {
  const emailConfig = await nconf.get('email')
  const html = `<div>
                  <p>亲爱的BPS用户，您好，</p>
                  <p>请点击下面的链接进行重置密码：</p>
                  <a href="${emailConfig.email_password_reset_url}?email=${email}&passwordtoken=${passwordToken}&uuid=${uuid}">
                    ${emailConfig.email_password_reset_url}?email=${email}&passwordtoken=${passwordToken}&uuid=${uuid}
                  </a>
                  <p>(如果链接无法点击，请将它复制并黏贴到浏览器的地址栏中访问)</p>
                  <p>为了保证您帐号的安全性，该链接有效期为24小时，并且点击一次后将失效!</p>
                  <p>如果您误收到此电子邮件，则可能是其他用户在尝试帐号设置时的误操作，如果您并未发起该请求，则无需再进行任何操作，并可以放心地忽略此电子邮件。</p>
                  </div>`
  return html
}

const getActivationHtmlLink = async (uuid, activateToken) => {
  const emailConfig = await nconf.get('email')

  const html = `<div>
                  <p>您好，欢迎使用Brand Protection Service，感谢您注册账号，点击下面的链接即可完成注册，链接24小时内有效：</p>
                  <a href="${emailConfig.email_activate_url}?uuid=${uuid}&activate_token=${activateToken}">
                    ${emailConfig.email_activate_url}?uuid=${uuid}&activate_token=${activateToken}
                  </a>
                  <p>(如果链接无法点击，请将它复制并黏贴到浏览器的地址栏中访问)</p>
                </div>`
  return html
}

/**
 * RESET password
 * @param {*} email
 * @param {*} passwordToken
 */
const emailPasswordResetToken = async ({ email, uuid, token: passwordToken }) => {
  const emailConfig = await nconf.get('email')
  const text = `To ${email}，
    Your password reset link:
    ${emailConfig.email_password_reset_url}?email=${email}&passwordtoken=${passwordToken}&uuid=${uuid}
    `

  const mailOptions = {
    from: `"simplyBrand, Inc" <${emailConfig.username}>`,
    to: email,
    subject: '密码重设通知 Password Reset E-mail',
    text: text,
    html: await getPasswordResetHtmlLink(email, uuid, passwordToken)
  }

  const emailTransporter = await transporter()

  emailTransporter.verify((error, success) => {
    if (error) {
      logger.info(error)
    } else {
      logger.debug('Server is ready to take our messages ' + success)
    }
  })

  const info = await emailTransporter.sendMail(mailOptions)
  logger.debug('[EMAIL] Message %s sent: %s', info.messageId, info.response)
  return info
}

/**
 * Activate account
 * @param {string} email
 * @param {string} uuid
 * @param {string} token
 */
const emailActivationToken = async ({ email, uuid, token: activateToken }) => {
  const emailConfig = await nconf.get('email')
  const text = `To ${email}，
    Your activation link:
    ${emailConfig.email_activate_url}?uuid=${uuid}&activate_token=${activateToken}
    `
  const mailOptions = {
    from: `"simplyBrand, Inc" <${emailConfig.username}>`,
    to: email,
    subject: '邮箱激活通知 E-mail activation',
    text: text,
    html: await getActivationHtmlLink(uuid, activateToken)
  }

  const emailTransporter = await transporter()

  emailTransporter.verify((error, success) => {
    if (error) {
      logger.info(error)
    } else {
      logger.debug('Server is ready to take our messages ' + success)
    }
  })

  const info = await emailTransporter.sendMail(mailOptions)
  logger.debug('[EMAIL] Message %s sent: %s', info.messageId, info.response)
  return info
}

const emailApplication = async (TaskID) => {
  const emailConfig = await nconf.get('email')
  const text = `有TaskID为${TaskID}的任务启动，请尽快确认！`
  const mailOptions = {
    from: `"simplyBrand, Inc" <${emailConfig.username}>`,
    to: emailConfig.task.toString(),
    subject: `BPS提醒您：有新任务启动`,
    text: text,
    html: `<div><p>${text}</p></div>`
  }
  const emailTransporter = await transporter()

  emailTransporter.verify((error, success) => {
    if (error) {
      logger.info(error)
    } else {
      logger.debug('Server is ready to take our messages ' + success)
    }
  })

  const info = await emailTransporter.sendMail(mailOptions)
  logger.debug('[EMAIL] Message %s sent: %s', info.messageId, info.response)
  return info
}

export default {
  resetToken: emailPasswordResetToken,
  sendToken: emailActivationToken,
  notify: emailApplication
}
