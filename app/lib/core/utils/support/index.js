import email from './email'
import sms from './sms'

export default {
  email,
  sms
}
