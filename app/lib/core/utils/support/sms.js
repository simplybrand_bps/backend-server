/* eslint no-console: ["error", { allow: ["log","error"] }] */
import { URL } from 'whatwg-url'
import fetch from 'node-fetch'
import nconf from 'nconf'
const logger = global.logger

const telephoneIsValid = (num) => {
  const chinaRule = /^[0-9]{11}$|^\+86[0-9]{11}$|^86[0-9]{11}$/g
  return chinaRule.test(num)
}

const constructUrl = async (mobiles, contents, sendtime) => {
  const smsConfig = await nconf.get('sms')
  return (
    'user=' +
    smsConfig.username +
    '&pwd=' +
    smsConfig.password +
    '&chid=' +
    smsConfig.chid +
    '&mobiles=' +
    mobiles +
    '&contents=' +
    contents +
    '&sendtime=' +
    (sendtime || '')
  )
}

const sendSms = async ({ phone, msg }) => {
  const smsConfig = await nconf.get('sms')
  const constructInfo = await constructUrl(phone, msg, null)
  return new Promise((resolve, reject) => {
    if (!telephoneIsValid(phone)) reject(new Error(`Error happens with the status code: 502, the key is: ${phone}`))
    const smsurl = new URL(smsConfig.sms_api_url + '?' + constructInfo) // send msg now
    logger.verbose('Sending URL: ' + smsurl)
    fetch(smsurl, { credentials: 'include', headers: { 'Content-Type': 'text/plain; charset=UTF-8', Connection: 'keep-alive' } })
      .then(res => {
        logger.verbose(res.status)
        if (res.ok) return res.text()
        else throw new Error('Error happens with the status code: 504, the key is: SMS')
      })
      .then(resbody => {
        logger.verbose('SMS Resp: ' + resbody)
        resolve(resbody)
      })
      .catch(err => reject(err))
  })
}

export default {
  sendSms
}
