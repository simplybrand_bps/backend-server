/**
 * 与app连通的唯一入口
 * 所有业务流程均在此模块中进行
 * 保证业务流程的变更不会对原app框架造成任何影响
 * 可在本模块中添加版本控制流程
 */

import express from 'express'
// import v0 from './main'
import apis from './core'

const router = express.Router()

// router.use(v0)
router.use(apis)

export default router
