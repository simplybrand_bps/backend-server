'use strict'

import cors from 'cors'

const whitelist = [
  'http://127.0.0.1:3002',
  'http://localhost:3002',
  'http://127.0.0.1:3000',
  'http://localhost:3000',
  'https://localhost:3443',
  'http://localhost:9000',
  'http://mynodsql-api.blcksync.info',
  'https://mynodsql-api.blcksync.info',
  'http://blcksync.com',
  'http://blcksync.info',
  'https://blcksync.com',
  'https://blcksync.info'
]

const corsOptionDelegate = (req, callback) => {
  let corsOptions
  // if incoming req origin is in the whitelist
  if (whitelist.indexOf(req.header('Origin')) !== -1) {
    corsOptions = { origin: true }
  } else {
    corsOptions = { origin: false }
  }
  callback(null, corsOptions)
}

const corsOptionDelegateWithCredentials = (req, callback) => {
  let corsOptions
  // if incoming req origin is in the whitelist
  if (whitelist.indexOf(req.header('Origin')) !== -1) {
    corsOptions = { origin: true, credentials: true }
  } else {
    corsOptions = { origin: false, credentials: true }
  }
  callback(null, corsOptions)
}

const corsWithOptions = cors(corsOptionDelegate)
const corsWithCredentialOptions = cors(corsOptionDelegateWithCredentials)

export {
  corsWithOptions,
  corsWithCredentialOptions
}
