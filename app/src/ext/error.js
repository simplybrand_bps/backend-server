'use strict'
const logger = global.logger

/**
 * 未处理的拒绝
 */
process.on('unhandledRejection', async (reason) => {
  await logger.warn(
    `unhandledRejection 
    ${process.stderr.fd}\n
    未处理的拒绝: ${reason.stack}\n`
  )
})

/**
 * 系统异常
 */
process.on('uncaughtException', (err) => {
  logger.error(
    `Crashed!\n uncaughtException 
    ${process.stderr.fd}\n
    捕获的异常: ${err.stack}`
  )
  process.exit(3)
})

/**
 * 处理app异常
 * @param {*} err
 */
const sysError = (err) => {
  logger.error(
    `Crashed!\n appError 
    ${process.stderr.fd}\n
    ${err.stack}`
  )
  process.exit(2)
}

/**
 * log process env
 */
logger.debug('NODE_ENV=' + process.env.NODE_ENV)
logger.debug('AUTH_TYPE=' + process.env.AUTH_TYPE)
/**
 * end
 */

export default sysError
