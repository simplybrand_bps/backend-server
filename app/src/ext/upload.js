'use strict'

import multer from 'multer'
import path from 'path'

const upload = multer({
  dest: path.join(__dirname, '../../public')
})

export default (fieldName) => {
  return upload.single(fieldName)
}
