'use strict'

import path from 'path'
import nconf from 'nconf'
import logger from './ext/logger'

global.logger = logger

const serverPort = async () => {
  try {
    const port = await nconf.get('port')
    if (!port) throw new Error('Undefined Port!')
    return port
  } catch (e) {
    logger.error(new Error('Crashed!\n Undefined NODE_ENV Setting!'))
    process.exit(0)
  }
}

/**
 * 监听server
 */
const serverStatus = {
  /**
   * 处理HTTP异常
   */
  onError: (err) => {
    if (err.syscall !== 'listen') {
      logger.warn(
        `listen 
        ${process.stderr.fd}\n
        ${err.stack}`
      )
    }
    switch (err.code) {
      case 'EACCES':
        logger.error(
          `Crashed!\n EACCES
          ${process.stderr.fd}\n
          ${err.stack}`
        )
        process.exit(1)
      case 'EADDRINUSE':
        logger.error(
          `Crashed!\n EADDRINUSE 
          ${process.stderr.fd}\n
          ${err.stack}`
        )
        process.exit(1)
      default:
        logger.warn(
          `serverError
          ${process.stderr.fd}\n
          ${err.stack}`
        )
    }
  },
  /**
   * 正常启动
   */
  onListening: (addr) => {
    const bind = typeof addr === 'string'
      ? 'pipe ' + addr
      : 'port ' + addr.port
    logger.info(`Listening on ${bind}`)
  }
}

/**
 * createServer失败
 * @param {*} err
 */
const initError = async (err) => {
  await logger.error(
    `Crashed!\n initError: 
    ${process.stderr.fd}\n
    ${err.stack}`
  )
  process.exit(0)
}

export default { serverPort, serverStatus, initError }
