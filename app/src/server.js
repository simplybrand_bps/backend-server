'use strict'
/**
 * APP相关应该在本文件中
 */
import express from 'express'
import path from 'path'
import cors from 'cors'
import helmet from 'helmet'
import compression from 'compression'

import { appError, indexRouter, middleware } from './ext'

const app = express()

try {
  app.use(helmet())
  app.use(cors())
  app.use(compression())
  app.use(express.json())
  app.use(express.urlencoded({ extended: false }))
  app.use('/static/files', express.static(path.join(__dirname, '../../public'))); 
  app.use(indexRouter)

  app.use(middleware.serverTimeout)
  app.use(middleware.pageNotFound)
  app.use(middleware.serverError)
} catch (e) {
  appError(e)
}

export default app
